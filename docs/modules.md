# Módulos y roles

En este apartado se describe a detalle a cada uno de los roles y módulos incluidos en la solución tecnológica MiMercadoAMG.

## Roles

La solución contempla 4 roles escenciales, mismos que se describen a continuación:

### Usuario /comprador

Rol asignado a los usuarios que deseen hacer uso de la aplicación para realizar sus compras del diario, que gustan de apoyar a la comunidad mediante el consumo local. Como compradores podrán seleccionar si desean recibir el pedido en su domicilio o pasar a recolectar al sitio, también podrá definir el método de pago que consideren conveniente.

### Vendedor

Rol asignado a los vendedores del mercado local, los usuarios con este rol podrán crear un espacio para su tienda que podrán configurar de acuerdo a la naturaleza de su negocio (métodos de pago, entregas). Además de hacer uso de las herramientas de captura de sus productos. También tendrán acceso al apartado de reportes, donde podrá consultar las estadísticas de su negocio.

### Repartidor

Rol asignado a los usuarios internos al mercado que brindarán el servicio de entrega, ellos podrán activar o desactivar su participación en cualquier momento. 
Este rol solo tiene participación mediante el uso de la aplicación móvil.

### Administrador general

Es el rol asignado al usuario que estará a cargo del monitoreo de la plataforma, así como a solventar las problemáticas relacionadas con el proceso de captura y flujos de la información.
Este rol solo tiene participación mediante el uso de la aplicación web.

## Módulos

La solución se estructura a manera de módulos, siendo estos accesibles desde la aplicación móvil o web, a continuación se describen los módulos y características principales incluidas en la plataforma:

### Registro de cuenta

La aplicación MiMercadoAMG es una solución que se adapta a los usuarios y muestra información personalizada según el comportamiento del mismo, también es necesario que proporcione información básica para poder realizar una compra, por tal motivo requiere de una cuenta de usuario, este módulo tiene la finalidad de proveer las funcionalidades necesarias para que un usuario pueda obtener y administrar una cuenta.

### Catálogo de categorías de productos

En este apartado el administrador general de la plataforma podrá capturar las categorías principales de productos para facilitar la búsqueda de productos.  
*Por ejemplo: Frutas y vegetales, Cereales, Pescados, Carne, Aves, Comida, Jugos, Lácteos, Hierbas y especias o Ropa y Accesorios*

### Catálogo de categorías de tiendas
En este apartado el administrador general de la plataforma podrá capturar las categorías o giros comerciales principales de tiendas  para facilitar la búsqueda de tiendas.  
*Por ejemplo: Alimentos preparados y/o bebidas, Alimentos no preparados (perecederos), Abarrotes y artículos misceláneos, 	Servicios (Cerrajería, Plomería, etc) y Otros*

### Administración de mercados
En este apartado el administrador general de la plataforma podrá agregar nuevos grupos de tiendas o mercados asignando un código de acceso y otra información relacionada.

### Administración de tiendas
En este apartado los vendedores podrán dar de alta una o varias tiendas para poner a disposición de los usuarios sus productos.
Requieren contar con el código de seguridad del mercado.

### Tiendas favoritas
En este apartado los compradores podrán visualizar las tiendas que han marcado previamente como favoritas.

### Administración de productos
En este apartado el vendedor podrá administrar sus productos y darlos de alta.

### Administración de precios

En este apartado se llevará un registro de los precios registrados por los vendedores para cada uno de sus productos, conservando un histórico de los mismos.

### Home / Página de inicio

En este apartado los usuarios podrán acceder a una pantalla de inicio donde podrán realizar la búsqueda de productos a comprar,  seleccionar la categoría de productos, tiendas y visualizar ofertas y promociones a través de un slide de imágenes.

### Búsquedas y filtros

En este apartado los usuarios podrán acceder a la búsqueda de productos a agregar a su carrito, estos podrán delimitar su búsqueda a través de facets  y filtros contextuales.

### Reviews y calificaciones

Con este componente los usuarios podrán realizar comentarios públicos y calificar una compra para que otros usuarios puedan conocer más acerca de la calidad del producto

### Preguntas y respuestas

Con este componente los usuarios podrán realizar preguntas concretas sobre un producto al vendedor, estas preguntas permanecerán públicas para que otros usuarios puedan conocer más acerca del producto.

### Carrito de compra

Este componente tiene la finalidad de permitir agregar los productos a comprar, permite revisar la compra y agregar o eliminar nuevos elementos antes de proceder al pago de los mismos, para cada producto de la orden se podrá agregar un comentario y por cada orden realizar un comentario general.

### Chat de compra

Por cada compra realizada el sistema habilitará un chat de comunicación entre el vendedor, el comprador y el repartidor para afinar cualquier detalle o imprevisto para el envío de la compra.

### Métodos de pago

Este componente / módulo permitirá al vendedor configurar sus métodos de pago disponibles y los detalles relacionados al pago de un producto de su tienda.

### Monitoreo de pedidos y envíos

Este módulo permitirá al comprador observar el estatus de sus pedidos

### Domicilios

Este módulo permitirá al comprador gestionar sus domicilios para la recepción de compras.

### Administración de pedidos y envíos

Este módulo permitirá al vendedor configurar sus métodos de envío y los detalles de envío.

### Administración de órdenes

Este módulo permitirá al vendedor controlar las órdenes recibidas, modificar el estatus de la misma para completar la orden o cancelarla.

### Administrar órdenes de envío

Este módulo / componente permitirá a los repartidores dar de alta su cuenta y hacer entregas a nombre de las tiendas.  
También gestionar las órdenes que esté realizando, así como observar los comentarios realizados en la orden de envío.

### Reportes

Este módulo desplegará la información sobre el comportamiento de las ventas realizadas por tienda, puede ser consultado por vendedores o por el administrador.

### Notificaciones

Este módulo tiene la finalidad de desplegar las notificaciones relacionadas a las acciones realizadas sobre la aplicación.

### Importaciones

Este módulo permitirá importar información de productos en la plataforma a los vendedores.

### Páginas básicas informativas

Este módulo permitirá capturar páginas básicas informativas con información estática.

### Formulario de contacto

Brinda a los usuarios un formulario de contacto para notificar al administrador sobre irregularidades o temas que consideren relevantes sobre el uso o comportamiento de la plataforma.

__*Fecha de actualización: 21-04-2022*__