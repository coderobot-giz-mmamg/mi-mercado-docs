# Guía de usuario aplicación móvil #

## 1\. Introducción ##

### 1.1 Alcance de este documento ###

El propósito de este manual es explicar de manera global el manejo de la app móvil Mi Mercado AMG, mostrando los tres tipos de usuario.

1.  Comprador
2.  Vendedor
3.  Repartidor

### 1.2 Cómo utilizar este manual ###

El presente manual ha sido redactado para ser un documento de referencia para los roles de usuario como compradores, vendedores y repartidores.

El manual está construido a partir de tres grandes temas:

1.  Distribución general: Corresponde a la explicación general de la app.
2.  Tipos de usuarios: Corresponde a los permisos que se tendrán dependiendo el rol.
3.  Explicación de cada rol: Comprador, vendedor y repartidor.

* * *

## 2\. Distribución general ##

### 2.1 Home ###

Al ingresar a la app cualquier usuario podrá navegar sin iniciar sesión, mostrando las siguientes regiones globales:

1.  Buscador: Permite al usuario buscar productos con filtros.
2.  Carrito de compras: Permite al usuario añadir distintos productos.
3.  Banners informativos: Permite al usuario consultar avisos realizados por administradores.
4.  Categorías:  Listado de categorías de productos.
5.  Tiendas: Listado de tiendas registradas en la app.
6.  Menú: Permite al usuario gestionar el uso de la app.

![Screenshot](images/user-app-guide/image26.png)
> Figura 1. Home público, Code Robot (2022).

El uso de la app sin iniciar sesión está limitado a realizar distintas funciones.

### 2.2 Inicio de sesión ###

La siguiente pantalla muestra el inicio de sesión, así como, las distintas opciones, ya sea para crear una cuenta nueva, iniciar sesión si ya se tiene una cuenta, iniciar sesión con Google o con Apple si se inicia desde un iPhone. Además, se puede seleccionar el perfil con el que se iniciará sesión, estos pueden ser:

*   Comprador
*   Vendedor
*   Repartidor

![Screenshot](images/user-app-guide/image51.png)
> Figura 2. Inicio de sesión, Code Robot (2022).

#### 2.2.1 Recuperación de contraseña ####

En caso de olvidar la contraseña, en el apartado anterior aparece una pregunta “¿Olvidaste tu contraseña?”, esta opción dirigirá al siguiente formulario de recuperación de contraseña, en donde se introduce el correo electrónico con el cual fue dada de alta la cuenta en la plataforma.

![Screenshot](images/user-app-guide/image1.png)
> Figura 3. Recuperación de contraseña, Code Robot (2022).


Al dar clic en el botón “Restablecer contraseña”, la plataforma enviará un correo electrónico con una llave temporal de acceso mediante el cual se podrá modificar la contraseña, por otra parte el sistema emitirá un mensaje al instante que se envió con éxito el correo de recuperación.

![Screenshot](images/user-app-guide/image27.png)
> Figura 4. Mensaje de recuperación de contraseña, Code Robot (2022).

* * *

## 3\. Cuenta ##

Una vez iniciada la sesión el usuario podrá ingresar a la plataforma e iniciar a comprar, vender o entregar pedidos, dependiendo su rol.

1.  Mi cuenta: Permite al usuario acceder al apartado de información de la cuenta.
2.  Edición: Permite al usuario acceder a la edición de su perfil.

![Screenshot](images/user-app-guide/image40.png)
> Figura 5. Menú de administración de cuenta, Code Robot (2022).

### 3.1 Edición de perfil ###

Permite al usuario modificar la información básica de su perfil, por ejemplo:

*   Nombre(s): Campo de texto, permite capturar de 1 a 50 caracteres, obligatorio.
*   Apellidos: Campo de texto, permite capturar de 1 a 50 caracteres, obligatorio.
*   Correo electrónico: Campo de email, permite capturar de 5 a 100 caracteres, obligatorio.

![Screenshot](images/user-app-guide/image35.png)
> Figura 6. Formulario de ajuste de cuenta, Code Robot (2022).

### 3.2 Contraseña ###

Permite al usuario modificar la contraseña de acceso a la plataforma, para poder realizar esta acción es necesario contar con la contraseña actual. Es un campo obligatorio.

Contraseña actual / nueva contraseña: Campo de texto, permite capturar de 4 a 50 caracteres.

![Screenshot](images/user-app-guide/image59.png)
> Figura 7. Formulario de ajuste de contraseña, Code Robot (2022).

## 4\. Tipos de usuario ##

La plataforma de Mi Mercado AMG tiene la capacidad de gestionar distintos tipos de roles de usuario, los cuales pueden ser compradores, vendedores y repartidores, cada uno de ellos tendrá facultades distintas para el manejo de la plataforma.

| Rol de usuario | Permisos |
|---|---|
| Comprador |- Consulta de productos<br>&nbsp;&nbsp;- Realizar preguntas sobre productos<br>- Compra de productos<br>- Segimiento de compra<br>&nbsp;&nbsp;- Estados de los pedidos<br>&nbsp;&nbsp;- Chat de atención<br>- Edición de mi cuenta<br>- Gestión de direcciones<br>- Creación de listas de tiendas favoritas<br>- Calificar vendedores<br>- Notificaciones|
| Vendedor | - Administrar tiendas propias<br>- Administración de pedidos y envíos<br>&nbsp;&nbsp;- Estados de los pedidos<br>&nbsp;&nbsp;- Chat de atención con comprador<br>- Administración de productos<br>- Administración de precios<br>- Administración de preguntas<br>- Edición de mi cuenta<br>- Notificaciones |
| Repartidor |- Administración de pool de órdenes<br>- Notificaciones<br>- Historial de entregas<br>- Edición de mi cuenta|

> Tabla 1.Tipos de usuarios, Code Robot (2022).

* * *

## 5\. Comprador ##

Al iniciar sesión como comprador aparecerá una pantalla de bienvenida, la cual, explica en cuatro pasos cómo realizar una compra, después de leer se sugiere dar clic en continuar.

![Screenshot](images/user-app-guide/image31.png)
> Figura 8. Pantalla de bienvenida, Code Robot (2022).


### 5.1 Home ###

La siguiente figura muestra la pantalla home, basada en metodologías de usabilidad para el fácil acceso a todos los componentes, a continuación se explica la función de cada uno de ellos:

1.  Buscador: Permite al usuario buscar productos con filtros.
2.  Carrito de compra: Permite al usuario consultar los distintos productos agregados al carrito.
3.  Banner informativo: Permite al usuario consultar avisos realizados por administradores.
4.  Listado de categorías: Se muestran todas las categorías de productos existentes.
5.  Consultados anteriormente: Muestra al usuario los productos o servicios consultados anteriormente, el usuario pudiera tener aún interés en adquirir.
6.  Tiendas: Listado de todas las tiendas disponibles.
7.  Últimos pedidos: Muestra al usuario sus últimos productos pedidos, esto para comprar nuevamente de una forma más rápida.
8.  Menú: Permite al usuario gestionar el uso de la app.  
    a.  Mi cuenta: Permite al usuario la gestión de cuenta, modificación de contraseña, adición de direcciones de envío, consultar políticas de privacidad, ayuda y cierre de sesión.  
    b.  Pedidos: Permite al usuario consultar sus pedidos actuales o el historial de pedidos.  
    c.  Notificaciones: Permite al usuario revisar notificaciones enviadas por el sistema, estas pueden ser del estatus de los pedidos, preguntas o chat del pedido.  
    d.  Home: Permite al usuario regresar al home o pantalla de inicio.

![Screenshot](images/user-app-guide/image30.png)
> Figura 9. Home con usuario logueado, Code Robot (2022).

Alternativamente a las opciones previamente listadas, también se pueden incluir el apartado de alertas, las cuales son definidas por el administrador, a fin de comunicar una situación relevante, como puede ser el recordatorio de las medidas de prevención sanitarias o una posible actualización disponible para la aplicación móvil.

### 5.2 Visualización de productos ###

#### Listado de categorías

Muestra el listado de categorías existentes para clasificar los productos.

![Screenshot](images/user-app-guide/image23.png)
> Figura 10. Listado de categorías, Code Robot (2022).

#### Buscador de productos

Esta herramienta permite al usuario realizar búsquedas de productos de distintas formas:  

1.  Buscador: Este tipo de búsqueda facilita cuando se requiere buscar por palabra clave o algún producto muy específico.

![Screenshot](images/user-app-guide/image6.png)
> Figura 11. Buscador, Code Robot (2022).

&ensp;2\.  Filtros: Permite al usuario realizar una búsqueda por distintas categorías, por ejemplo:  
&emsp;&emsp;a\.  Mercado  
&emsp;&emsp;b\.  Tienda  
&emsp;&emsp;c\.  Tipo de tienda  
&emsp;&emsp;d\.  Categoría de producto  
&emsp;&emsp;e\.  Métodos de envío  
&emsp;&emsp;f\.  Método de pago  
&emsp;&emsp;g\.  Pickup  
&emsp;&emsp;h\.  Entrega a domicilio  
&emsp;&emsp;i\.  Acepta tarjeta en local  

![Screenshot](images/user-app-guide/image8.png)
> Figura 12. Filtro, Code Robot (2022).

&ensp;3\.  Filtros: Permite al usuario filtrar por nombre, precio o calificación.

![Screenshot](images/user-app-guide/image38.png)
> Figura 13. Filtro ascendentes, Code Robot (2022).

#### Ficha de producto

Permite al usuario conocer la información completa del producto, tal como el precio, quién la vende, calificación del vendedor, así como (1) añadir al carrito para poder comprar y (2) poder preguntar al vendedor sobre el producto en caso de alguna duda.

![Screenshot](images/user-app-guide/image54.png)
> Figura 14. Ficha de producto, Code Robot (2022).

Por otra parte, existirán en la plataforma diversos servicios o productos los cuales no podrán tener un precio asignado por su naturaleza, por lo tanto tendrán que ser cotizados por el vendedor.

![Screenshot](images/user-app-guide/image33.png)
> Figura 15. Ficha de showroom, Code Robot (2022).

#### Preguntas

A partir de la ficha del producto el usuario podrá realizar preguntas sobre el producto al vendedor como se muestra en la siguiente figura:

![Screenshot](images/user-app-guide/image53.png)
> Figura 16. Preguntas de productos, Code Robot (2022).

#### Reseñas

A partir de la ficha del producto el usuario podrá ingresar al apartado de reseñas, el cual permite al comprador conocer más sobre varios temas del vendedor, estos comentarios pueden ser sobre la atención, productos en general, así como comentarios que ayuden a mejorar al vendedor, etc.

![Screenshot](images/user-app-guide/image2.png)
> Figura 17. Reseñas de compras, Code Robot (2022).

### 5.3 Visualización de tiendas ###

#### Listado de tiendas

Este apartado permite visualizar el listado de tiendas de mejor manera, acomodada por categorías y por mis tiendas favoritas, estas se pueden añadir como favoritas al entrar a la ficha de la tienda y dar clic en el corazón.

![Screenshot](images/user-app-guide/image18.png)
> Figura 18. Listado de tiendas, Code Robot (2022).

#### Ficha de tienda

La ficha de tienda permite al comprador conocer en su totalidad el negocio, desde sus productos, su historia hasta sus calificaciones. Como se menciona en el apartado anterior en el lado superior derecho el usuario podrá añadir a favoritos las tiendas de su preferencia.

![Screenshot](images/user-app-guide/image3.png)
> Figura 19. Ficha de tienda, Code Robot (2022).

### 5.4 Pedidos ###

Este apartado de la app es uno de los ejes principales, es donde el usuario puede consultar los detalles de sus órdenes o pedidos en curso o así mismo el historial de los ya entregados, para llegar a este apartado el usuario tiene que dar clic en “Pedidos”, se encuentra en la barra de menú inferior de color verde.

![Screenshot](images/user-app-guide/image50.png)
> Figura 20. Listado de pedidos, Code Robot (2022).

#### Estatus de compra

Después de dar clic en detalles de la orden, la app desplegará la información completa del estatus del pedido, este estará actualizando en cada una de sus etapas. En caso de tener alguna duda o comentario el comprador podrá enviar un mensaje o llamar al vendedor.

![Screenshot](images/user-app-guide/image22.png)
> Figura 21. Estatus de pedidos, Code Robot (2022).

#### Detalle de la compra

Al dar clic en el detalle de compra se desplegará la información completa de la compra, esto es el producto, lo que se pagó y en caso de ser entrega a domicilio se mostrará también la dirección o si no cómo recoger en mercado.

![Screenshot](images/user-app-guide/image13.png)
> Figura 22. Detalle de compra, Code Robot (2022).

#### Calificar compra

Al ser entregado el pedido, el comprador podrá calificar al vendedor por su atención y también dejar estrellas sobre los productos adquiridos.

![Screenshot](images/user-app-guide/image21.png)
> Figura 23. Calificar compra, Code Robot (2022).

### 5.5 Carrito de compras ###

El carrito de compras permite al usuario añadir distintos productos, estos pueden ser de un solo vendedor, por otra parte, en algunos productos existirán variantes las cuales podrán ser seleccionadas dependiendo el interés del cliente, un ejemplo puede ser el sabor de un producto, se debe tomar en cuenta que el vendedor deberá habilitar este apartado. Posterior a la revisión de todos los productos dentro del carrito, se podrá continuar con la compra.

![Screenshot](images/user-app-guide/image5.png)
> Figura 24. Carrito de compras, Code Robot (2022).

#### Detalles de envío

Después de dar clic en continuar en el carrito de compra,  este será el segundo paso, en el cual, aparecerá el selector de método de entrega y dirección de entrega.

El comprador deberá considerar lo siguiente para las opciones de método de entrega:

1.  Entrega a domicilio: El usuario deberá seleccionar un domicilio que esté dentro del área de cobertura para poder continuar con la compra.
2.  Ordena y recoge: El usuario se compromete a acudir por el pedido al mercado y local del vendedor.
3.  Área de cobertura: Permite al usuario consultar el polígono de entrega a domicilio del mercado.

![Screenshot](images/user-app-guide/image47.png)
> Figura 25. Carrito de compras 2, Code Robot (2022).

#### Agregar dirección de envío

El usuario podrá añadir distintas direcciones de envío dando clic en el botón de “Agregar dirección” , esto desde el carrito de compra o ya sea desde “Mi cuenta”. Se debe considerar que para poder recibir las compras a domicilio es requerido estar dentro del área de reparto que definió el mercado.

![Screenshot](images/user-app-guide/image9.png)
> Figura 26. Agregar dirección, Code Robot (2022).

#### Resumen de compra

Posterior a los pasos anteriores, la app mostrará el resumen de la compra con todas las opciones seleccionadas, listo para finalizar la compra.

![Screenshot](images/user-app-guide/image32.png)
> Figura 27. Resumen de compra, Code Robot (2022).

#### Compra exitosa

Al finalizar la compra la app mostrará al comprador la confirmación de compra.

![Screenshot](images/user-app-guide/image7.png)
> Figura 28. Compra exitosa, Code Robot (2022).


### 5.6 Notificaciones ###

La app mandará notificaciones de actualizaciones de pedidos, las cuales pueden ser consultadas en el apartado de “Notificaciones” que aparece en el menú inferior de color verde.

![Screenshot](images/user-app-guide/image37.png)
> Figura 29. Notificaciones del comprador, Code Robot (2022).

### 5.7 Cuenta ###

1.  Mi cuenta: Permite al usuario acceder al apartado de información de la cuenta.
2.  Edición: Permite al usuario acceder a la edición de su perfil.

![Screenshot](images/user-app-guide/image10.png)
> Figura 30. Mi cuenta comprador, Code Robot (2022).

## 6\. Vendedor ##

### 6.1 Home ###

La siguiente figura muestra la pantalla home del vendedor, la cual está basada en metodologías de usabilidad para el fácil acceso a todos los componentes administrativos para los negocios. A continuación se explica brevemente cada uno de los apartados:

1.  Administrar tiendas: Permite al usuario añadir o administrar su negocio.
2.  Administrar pedidos y envíos: Permite al usuario tomar órdenes de los compradores y darles seguimiento.
3.  Administrar productos: Permite al usuario añadir o editar productos o servicios de su negocio.
4.  Administrar precios: Permite actualizar los precios de los productos existentes.
5.  Administrar preguntas: Permite al usuario responder a las preguntas de los compradores.
6.  Menú: Permite al usuario gestionar el uso de la app.  
    a.  Mi cuenta: Permite al usuario la gestión de cuenta, modificación de contraseña, consultar políticas de privacidad, ayuda y cierre de sesión.  
    b.  Pedidos: Permite al usuario consultar sus pedidos actuales o el historial de pedidos.  
    c.  Notificaciones: Permite al usuario revisar notificaciones enviadas por el sistema, estas pueden ser órdenes solicitadas.  
    d.  Home: Permite al usuario regresar al home o pantalla de inicio.

![Screenshot](images/user-app-guide/image46.png)
> Figura 31. Home vendedor, Code Robot (2022).


### 6.2 Administrar tiendas ###

Este módulo permite al vendedor editar y añadir nuevas tiendas, así como, consultar productos, órdenes y preguntas.

Al acceder a este módulo de “Administración de tiendas”  la plataforma desplegará una pantalla donde se mostrará ya sea el listado de tiendas previamente capturadas o la creación de una nueva tienda.

![Screenshot](images/user-app-guide/image24.png)
> Figura 32. Administración de tiendas, Code Robot (2022).


#### Crear nueva tienda

Al ingresar al formulario de crear tienda, la plataforma mostrará la primera parte de captura de la misma, los campos se presentan en la siguiente figura:

*   Nombre del negocio: El vendedor deberá escribir el nombre de su negocio.
*   Mercado: Seleccionar el nombre del mercado donde se encuentra el negocio.
*   Código de seguridad: Este código será proporcionado por el administrador o responsable de la plataforma.

![Screenshot](images/user-app-guide/image55.png)
> Figura 33. Formulario de creación de tienda 1,  Code Robot (2022).

Una vez capturada correctamente la información anterior, se mostrará la segunda parte del formulario de captura de una nueva tienda. La siguiente figura muestra los campos:

*   Dirección o número de local: El vendedor deberá indicar el lugar donde pueden pasar a recoger sus pedidos los usuarios.
*   Horario de atención: Este campo hace referencia al horario de atención a pedidos en la plataforma, este no necesariamente tiene que ser el horario real en el que habitualmente abre el negocio.
*   Métodos de entrega: El vendedor deberá seleccionar al menos un método de entrega, puede ser a domicilio o entrega en mercado (pickup).
*   Repartidores asignados: Al haber seleccionado entrega a domicilio, el vendedor podrá seleccionar sus propios repartidores previamente dados de alta.
*   Repartidores globales: Hace referencia a los repartidores proporcionados por el mercado, esto en caso de no contar con repartidores exclusivos.
*   Política de envío: Este apartado permite al vendedor especificar alguna política de sus envíos, estos pueden ser los tiempos de entrega, etc.
*   Entregas en mercado (pickup): Este es el segundo método de entrega, el cual hace referencia a que los compradores podrán pasar al local por sus pedidos listos.
*   Estado de la tienda: Este campo podrá ser marcado como activo si se quiere que la tienda sea visible para los usuarios en la plataforma.
*   Formas de pago aceptadas: Este campo es un checkbox donde se podrán seleccionar los métodos de pago válidos en la tienda, estos pueden ser efectivo o pasarela de pago para pagos en línea.
*   Terminal punto de venta: Marcar como activado en caso de contar con terminal de pagos en punto de venta.
*   Imagen / logo: Permite al usuario subir el logo del negocio, en caso de no contar con logo, pudiera subir alguna foto del negocio como referencia visual para los compradores.
*   Giro / ramo industrial: Seleccionar el ramo en el cual se ubica el negocio.
*   Bloquear entrada de nuevas órdenes: Permite al vendedor cerrar la opción de que sus productos puedan ser añadidos al carrito de compra.
*   Configurar horario de atención: Permite calendarizar el horario de atención del negocio.
*   Configurar horario de envío a domicilio: Permite al usuario añadir horario específico de envío de productos a domicilio.
*   Guardar: Permite al usuario guardar la tienda con todos los campos ya capturados.

![Screenshot](images/user-app-guide/image52.jpg)
> Figura 34. Formulario de creación de tienda 2, Code Robot (2022).

### 6.3 Administrar pedidos y envíos ###

Este  módulo permite al vendedor consultar el listado de órdenes que han sido creadas por los usuarios, inicialmente estarán filtradas por aquellas que no deben ser atendidas o están en proceso para darles seguimiento es necesario dar clic en “Atender”. Para poder gestionar pedidos es necesario seleccionar la tienda a administrar.

![Screenshot](images/user-app-guide/image41.jpg)
> Figura 35. Administración de pedidos y envíos, Code Robot (2022).

En el apartado de filtros de los pedidos el vendedor podrá filtrar por estados o por agrupaciones de atendidos o por atender.

![Screenshot](images/user-app-guide/image11.png)
> Figura 36. Administración de pedidos y envíos, Code Robot (2022).

Dentro del detalle de orden el vendedor podrá realizar las siguientes acciones:

*   Detalles del pedido: Permite al vendedor consultar los detalles del pedido, tal como, el método de entrega, número de pedido, domicilio, método de pago, etc.
*   Lista de productos solicitados: Muestra todos los productos del pedido, cantidad a despachar y el precio final.
*   Chat con el cliente: Esta herramienta permite al comprador y vendedor preguntar dudas y tener comunicación directa para una mejor experiencia de usuario.
*   Llamada con el cliente: El vendedor podrá dar clic en “Llamada telefónica” para hablar con el cliente.

![Screenshot](images/user-app-guide/image15.png)
> Figura 37. Pedido en curso, Code Robot (2022).

*   Selector de estatus: Permite la selección del listado predeterminado de actualización de estatus.  
    *   Orden creada  
    *   Orden aceptada  
    *   Preparando orden  
    *   Despachando orden  
    *   En espera de repartidor  
    *   Tu pedido está en camino  
    *   En puerta con el cliente  
    *   Entregado
*   Agregar mensajes de estatus: Permite al vendedor agregar alguna nota opcional en cada cambio de estatus de la orden.
*   Actualizar estatus: Después de seleccionar el estatus correcto el vendedor deberá dar clic en el botón de “Actualizar estatus”.

![Screenshot](images/user-app-guide/image14.png)
> Figura 38. Pedido en curso 2, Code Robot (2022).

### 6.4 Administrar productos ###


Permite a los vendedores añadir o editar productos  o servicios de sus negocios,  a través de un formulario muy detallado, con campos muy específicos. Para poder visualizar y gestionar productos es necesario seleccionar una tienda en la parte superior.

![Screenshot](images/user-app-guide/image25.png)
> Figura 39. Administración de productos, Code Robot (2022).

![Screenshot](images/user-app-guide/image49.png)
> Figura 40. Formulario de captura de producto 1, Code Robot (2022).


*   Showroom: Seleccionar el check si lo capturado corresponde a un servicio el cual no cuente con un precio fijo.
*   Variantes y componentes: Permite añadir variantes del mismo producto que no afecten al precio, por ejemplo, chilaquiles; Categoría  = “Sabor”, Variante(s) = “rojos, verdes”.

![Screenshot](images/user-app-guide/image34.jpg)
> Figura 41. Formulario de captura de producto 2, Code Robot (2022).


*   Impuestos del producto: Se le da la opción al vendedor de optar por cobrar impuestos por separado, esto no es obligatorio por lo cual podría sumarse directamente al precio y omitir este campo.

![Screenshot](images/user-app-guide/image4.png)
> Figura 42. Formulario de captura de producto 3, Code Robot (2022).


### 6.5 Administrar precios ###

Permite al vendedor actualizar de manera muy fácil y sencilla el precio de los productos existentes de su tienda.  Para poder visualizar y gestionar precios es necesario seleccionar una tienda en la parte superior.

![Screenshot](images/user-app-guide/image20.png)
> Figura 43. Administración de precios, Code Robot (2022).

### 6.6 Administrar preguntas ###

Permite al vendedor responder a las preguntas de los posibles clientes sobre los distintos productos ofertados. Para poder visualizar y gestionar preguntas es necesario seleccionar una tienda en la parte superior.

![Screenshot](images/user-app-guide/image58.png)
> Figura 44. Administración de preguntas, Code Robot (2022).

![Screenshot](images/user-app-guide/image12.png)
> Figura 45. Responder preguntas, Code Robot (2022).

### 6.7 Notificaciones ###

La app mandará notificaciones de actualizaciones de nuevos pedidos, mensajes de chat, calificaciones de venta y preguntas, las cuales pueden ser consultadas en el apartado de “Notificaciones” que aparece en el menú inferior de color verde.

![Screenshot](images/user-app-guide/image19.png)
> Figura 46. Listado de notificaciones del vendedor, Code Robot (2022).

### 6.8 Cuenta ###

El usuario podrá ingresar a la plataforma y según sus permisos asignados podrá hacer uso de los módulos de administración que se presentan en la siguiente figura:

![Screenshot](images/user-app-guide/image44.png)
> Figura 47. Menú de administración de cuenta, Code Robot (2022).

* * *

## 7\. Repartidor ##

Al iniciar sesión como repartidor se debe considerar que el administrador de la plataforma será quien active las cuentas de repartidor, de otra manera no podrán acceder a la función.

![Screenshot](images/user-app-guide/image29.png)
> Figura 48. Pantalla de activación de cuenta, Code Robot (2022).

### 7.1 Home ###

Posterior a la activación de cuenta el repartidor podrá ingresar a la pantalla home, la cual está basada en metodologías de usabilidad para el fácil acceso a todos los componentes administrativos para las entregas. A continuación se explica brevemente cada uno de los apartados:

*   Pool de órdenes: Permite al repartidor entrar al listado de órdenes por entregar.
*   Historial de entregas: El repartidor podrá consultar los pedidos entregados anteriormente.
*   Menú: Permite al usuario gestionar el uso de la app.  
    *   Mi cuenta: Permite al usuario la gestión de cuenta, modificación de contraseña, consultar políticas de privacidad, ayuda y cierre de sesión.  
    *   Notificaciones: Permite al usuario revisar notificaciones enviadas por el sistema, estas pueden ser órdenes para ser entregadas.  
    *   Home: Permite al usuario regresar al home o pantalla de inicio.

![Screenshot](images/user-app-guide/image17.png)
> Figura 49. Home del repartidor, Code Robot (2022).

### 7.2 Pool de órdenes y órdenes en proceso ###

#### Órdenes en proceso

Este módulo permite al repartidor administrar las órdenes en proceso, es decir, aquellas que él decidió atender y deberá darle seguimiento. Para esto en el apartado de orden en proceso es necesario dar clic en “Detalle”.

![Screenshot](images/user-app-guide/image45.png)
> Figura 50. Pool de órdenes a entregar, Code Robot (2022).


#### Detalle de orden

Posterior a darle clic en detalle, en la pantalla aparecerá el pedido en curso, donde habrá distintos estatus, los cuales el repartidor en todo momento deberá actualizar para informar al comprador y vendedor sobre su entrega.

Al aceptar la orden en automático se actualiza el estatus de “Pedido en camino”. En la parte superior se encuentra el campo de comentarios o información adicional, ahí pueden venir ciertas peticiones especiales de los clientes. Al recolectar el pedido se sugiere como buena práctica revisar el pedido antes de llevarlo al domicilio.

![Screenshot](images/user-app-guide/image43.png)
> Figura 51. Pedidos en curso, Code Robot (2022).

Posterior a recolectar el pedido el repartidor deberá actualizar  los siguientes estatus en  la app:

En puerta con el cliente: Este estatus deberá actualizarse cuando el repartidor esté afuera del inmueble del cliente.

Entregado: Actualizar este estatus al entregar al cliente el pedido.

![Screenshot](images/user-app-guide/image48.png)
> Figura 52. Estatus a actualizar, Code Robot (2022).

#### Pool

Este módulo permite al repartidor ver un listado de las órdenes que los vendedores tienen listas para enviar. Para apropiarse de la entrega el repartidor deberá seleccionar una del listado y seleccionar “Tomar” y confirmar la solicitud. Una vez realizado esto, la orden en cuestión se trasladará al apartado de “Órdenes en proceso”.

![Screenshot](images/user-app-guide/image39.png)
> Figura 53. Estatus a actualizar, Code Robot (2022).

### 7.3 Historial de entregas ###

Permite al repartidor consultar el historial de entregas realizadas.

![Screenshot](images/user-app-guide/image42.png)
> Figura 54. Historial de entregas, Code Robot (2022).

### 7.4 Notificaciones ###

La app mandará notificaciones de chats con los clientes para resolución de dudas, las cuales pueden ser consultadas en el apartado de “Notificaciones” que aparece en el menú inferior de color verde.

![Screenshot](images/user-app-guide/image57.png)
> Figura 55. Notificaciones del repartidor, Code Robot (2022).

### 7.5 Cuenta ###

1.  Mi cuenta: Permite al usuario acceder al apartado de información de la cuenta.
2.  Edición: Permite al usuario acceder a la edición de su perfil.

![Screenshot](images/user-app-guide/image28.png)
> Figura 56. Menú de administración de cuenta repartidor, Code Robot (2022).

__*Fecha de actualización: 21-04-2022*__