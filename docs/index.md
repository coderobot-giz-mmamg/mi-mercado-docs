# MiMercadoAMG

Es una solución digital que cuenta con acceso web y móvil, tanto iOS como Android que permite a los vendedores de un mercado publicar sus productos y venderlos a través de distintos esquemas que consideran pago en efectivo, línea, y entrega pickup, así como envíos a domicilio, enfocada en la resolución de los siguientes objetivos estratégicos:

![Screenshot](images/overview/img-001.png)


## Componentes

La solución se conforma de dos componentes principales, mismos que se describen a continuación:

### MiMercadoAMG Web

Plataforma web del mercado desde la cual los usuarios podrán realizar consultas o compras, también permite a los vendedores gestionar sus tiendas, productos e inclusive dar seguimiento a los pedidos que los usuarios les realicen, a su vez permitirá a los administradores tener acceso a los módulos de configuración y  gestionar contenidos existentes en la plataforma.

### MiMercadoAMG Móvil

La aplicación móvil integra las funcionalidades y herramientas, que permitirán a los usuarios, vendedores y repartidores interactuar en un entorno de comercio electrónico y distribución.
 
## Arquitectura tecnológica de la solución

En este apartado se detallan las tecnologías utilizadas en la solución tecnológica MiMercadoAMG y su arquitectura.

![Screenshot](images/overview/img-002.jpg)

### Angular
Angular (comúnmente llamado "Angular 2+" o "Angular 2"), es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página. Su objetivo es aumentar las aplicaciones basadas en navegador con capacidad de Modelo Vista Controlador (MVC), en un esfuerzo para hacer que el desarrollo y las pruebas sean más fáciles.

### Ionic
IONIC es un SDK de frontend de código abierto para desarrollar aplicaciones híbridas basado en tecnologías web (HTML, CSS y JS). Este es un framework que nos permite desarrollar aplicaciones para iOS nativo, Android y la web, desde una única base de código.

### Nginx
Nginx es un servidor web/proxy inverso ligero de alto rendimiento y un proxy para protocolos de correo electrónico (IMAP/POP3).

### Spring
Spring es un framework para el desarrollo de aplicaciones y contenedor de inversión de control, de código abierto para la plataforma Java.

### Minio
Minio es un servidor de almacenamiento de objetos y archivos.

### Postgresql
PostgreSQL es un sistema de gestión de bases de datos relacional orientado a objetos y de código abierto, publicado bajo la licencia PostgreSQL,​ similar a la BSD o la MIT.

### Postgis
PostGIS convierte al sistema de administración de bases de datos PostgreSQL en una base de datos espacial mediante la adición de tres características: tipos de datos espaciales, índices espaciales y funciones que operan sobre ellos.

### Elastic search
ElasticSearch es un servidor de búsqueda basado en Lucene. Provee un motor de búsqueda de texto completo, distribuido y con capacidad de multitenencia con una interfaz web RESTful y con documentos JSON.

__*Fecha de actualización: 21-04-2022*__