# Extensión y customización de la solución #

La solución tecnológica MiMercadoAMG se construye a partir del proyecto JHipster, un generador de aplicaciones de código abierto que se utiliza para desarrollar rápidamente aplicaciones web modernas utilizando Angular y Spring Framework. El utilizar Spring Framework facilita la documentación en código y provee un marco de desarrollo fácil de entender y extender gracias a la utilización de patrones de diseño probados a través del tiempo.

## Patrones de diseño utilizados ##

1. Model View Controller (MVC)
2. Front Controller Design Pattern
3. View Helper
4. Dependency injection or inversion of control (IOC)
5. Factory Design Pattern
6. Proxy Design Pattern
7. DTO / Repository


## Tecnologías del stack ##

- Spring Boot
- Maven 
- Spring Security
- Spring MVC REST + Jackson
- Spring Data JPA + Bean Validation
- Database updates with Liquibase
- Angular 10
- Ionic 5
- POstgresSQL
- Elasticsearch


## Descripción del diseño de software ##

Para conocer más detalles de la arquitectura de la solución, solicitar acceso a la __'Descripción del diseño de software'__ y al __'código fuente de la solución'__ a los administradores de la solución.

## Documentación relacionada ##

- https://www.jhipster.tech/development/  

__*Fecha de actualización: 21-04-2022*__