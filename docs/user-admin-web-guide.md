#  Guía del administrador web #

## 1. Introducción ##


### 1.1 Alcance de este documento ###

El propósito de este manual es explicar de manera detallada el manejo administrativo de la solución tecnológica de Mi Mercado AMG.

### 1.2 Cómo utilizar este manual ######

El presente manual ha sido redactado para ser un documento de referencia para el rol de usuario administrador.

El manual está construido a partir de tres grandes temas:

1.  Distribución general: corresponde a la explicación general de la plataforma pública, así como, el inicio de sesión y recuperación de cuenta.  
2.  Gestión de usuarios / permisos: corresponde a los permisos que se tendrán dependiendo el rol asignado.
3.  Explicación de conjuntos de módulos: Especificación de funcionalidades particulares de cada módulo.

Cabe mencionar que para efectos de desarrollo del presente manual todas las figuras y explicaciones de flujo están basadas en el tipo de usuario “Administrador”.

* * *

## 2\. Distribución general ##


### 2.1 Página principal ###

La plataforma se compone de tres regiones principales:

La primera es el encabezado de la página principal, esta muestra el logo de Mi Marcado AMG, carrusel de banners para publicaciones importantes, carrito de compra, buscador de productos, así como el acceso directo para el inicio de sesión.

![Screenshot](images/admin-web-guide/image72.png)
> Figura 1. Encabezado, Code Robot (2022).  

La segunda es el contenido, este apartado muestra las categorías y tiendas vistas anteriormente.

![Screenshot](images/admin-web-guide/image65.png)
> Figura 2. Contenido público del home, Code Robot (2022).

La tercera parte es el pie de página, el cual mostrará la información de contacto de Mi Mercado AMG.

![Screenshot](images/admin-web-guide/image39.png)
> Figura 3. Pie de página, Code Robot (2022).

### 2.2 Inicio de sesión ###

Los usuarios con cuenta podrán acceder a la plataforma a través de la opción “inicio de sesión”, la cual se muestra en el header del sitio.

![Screenshot](images/admin-web-guide/image47.png)
> Figura 4. Botón inicio de sesión, Code Robot (2022).

Al ejecutar esta opción la plataforma mostrará una ventana donde se podrán introducir los datos de acceso del usuario.

![Screenshot](images/admin-web-guide/image63.png)
> Figura 5. Ventana inicio de sesión, Code Robot (2022).

1.  **Tipo de usuario “Comprador”**
2.  **Tipo de usuario “Vendedor”**
3.  **Tipo de usuario “Otro”** (Ej. administradores)
4.  **Inicio de sesión**
5.  **Contraseña**
6.  **Recuperar contraseña**
7.  **Crear cuenta**
8.  **Inicio de sesión con Google**
9.  **Inicio de sesión con Apple**

#### 2.2.1 Recuperación de contraseña #######


En caso de olvidar la contraseña, en el apartado anterior aparece una pregunta “¿Olvidaste tu contraseña?”, esta opción dirigirá al siguiente formulario de recuperación de contraseña, en donde se introduce el correo electrónico con el cual fue dada de alta la cuenta en la plataforma.

![Screenshot](images/admin-web-guide/image45.png)
> Figura 6. Recuperación de contraseña, Code Robot (2022).

Al dar clic en el botón “Restablecer contraseña”, la plataforma enviará un correo electrónico con una llave temporal de acceso mediante el cual se podrá modificar la contraseña, por otra parte el sistema emitirá un mensaje al instante que se envió con éxito el correo de recuperación.

![Screenshot](images/admin-web-guide/image49.png)
> Figura 7. Mensaje de recuperación de contraseña, Code Robot (2022).

* * *

## 3\. Cuenta ##


El usuario podrá ingresar a la plataforma y según sus permisos asignados podrá hacer uso de los módulos que se presentan a continuación:

![Screenshot](images/admin-web-guide/image58.png)
> Figura 8. Menú de administración de cuenta, Code Robot (2022).

### 3.1 Ajustes ###

Permite al usuario modificar la información básica de su perfil, por ejemplo:

*   Nombre(s): Campo de texto, permite capturar de 1 a 50 caracteres, obligatorio.
*   Apellidos: Campo de texto, permite capturar de 1 a 50 caracteres, obligatorio.
*   Teléfono: Campo numérico, obligatorio.
*   Correo electrónico: Campo de email, permite capturar de 5 a 100 caracteres, obligatorio.

![Screenshot](images/admin-web-guide/image42.png)
> Figura 9. Menú de administración de cuenta, Code Robot (2022).

### 3.2 Contraseña ###

Permite al usuario modificar la contraseña de acceso a la plataforma, para poder realizar esta acción es necesario contar con la contraseña actual. Es un campo obligatorio.

Contraseña actual / nueva contraseña: Campo de texto, permite capturar de 4 a 50 caracteres.

![Screenshot](images/admin-web-guide/image23.png)

> Figura 10. Formulario de ajuste de contraseña, Code Robot (2022).

### 3.3 Cerrar sesión ######

Finaliza la sesión del usuario dentro de la plataforma, al hacerlo la plataforma redireccionará a la página principal del sitio.

* * *

## 4\. Gestión de usuarios ##


La plataforma de Mi Mercado AMG tiene la capacidad de gestionar distintos tipos de roles de usuario, los cuales, pueden ser: administradores, compradores, vendedores y repartidores, cada uno de ellos tendrán facultades distintas para el manejo de la plataforma.

Para la captura de nuevos usuarios o edición de los existentes, el administrador desde el home podrá acceder dando clic en administración, gestión de usuarios y seguir los pasos siguientes:

&ensp;1\.  Menú dropdown: El usuario podrá desplegar el menú de administración y dar clic en Gestión de usuarios.  

![Screenshot](images/admin-web-guide/image3.png)
> Figura 11. Menú dropdown, gestión de usuarios, Code Robot (2022).

&ensp;2\.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
*   Búsqueda por Login
*   Búsqueda por Nombre completo
*   Búsqueda por Rol de usuario

Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.

&ensp;3\.  Creación de usuarios: Este punto se verá a detalle en el apartado 4.1 Creación o edición de usuarios.

![Screenshot](images/admin-web-guide/image30.png)
> Figura 12. Módulo gestión de usuarios, Code Robot (2022).

### 4.1 Creación o edición de usuarios ###

Este apartado permite al administrador la creación de nuevos usuarios o edición de usuarios existentes, a través de un formulario de captura, el cual desplegará la siguiente distribución de los campos.

![Screenshot](images/admin-web-guide/image27.png)
> Figura 13. Formulario creación de usuario, Code Robot (2022).

Posterior al llenado de los campos solicitados, en el apartado de perfiles (4) el administrador podrá seleccionar el tipo de rol que desempeñará el usuario.

En caso de que el Usuario fuese un repartidor el administrador deberá:  
*   Verificar si el repartidor pertenece a una tienda en particular o es un repartidor global.
*   Seleccionar el mercado al cual pertenece el repartidor.
*   Editar la cuenta del usuario de acuerdo a la información anterior y activar cuenta.

Si el repartidor pertenece a una tienda en particular, el dueño de la tienda deberá activar la opción “Envío a domicilio” en su configuración de tienda y añadir al nuevo usuario a su lista de repartidores exclusivos.

Guardar (5): Al finalizar con la captura o edición de los campos, el administrador podrá guardar lo realizado.  

### 4.2 Tipos de usuarios ######


| Rol de usuario | Permisos |
|---|---|
| Administrador | - Acceso completo a todos los apartados de la plataforma, así como, todos los permisos para realizar cualquier acción |
| Comprador | - Consulta de productos<br>&nbsp;&nbsp;- Realizar preguntas sobre productos<br>- Compra de productos<br>- Seguimiento a compras<br>&nbsp;&nbsp;- Estado de la compra<br>&nbsp;&nbsp;- Chat de atención<br>- Edición de mi cuenta<br>- Administrar direcciones de envío <br>- Creación de listas de tiendas favoritas<br>- Calificar vendedores<br>- Notificaciones |
| Vendedor | - Administrar tiendas propias<br>- Administración de pedidos y envíos<br>&nbsp;&nbsp;- Estado de la compra<br>&nbsp;&nbsp;- Chat de atención<br>- Administración de productos<br>- Administración de precios<br>- Administración de preguntas<br>- Edición de mi cuenta<br>- Notificaciones |
| Repartidor | - Acceso únicamente a la app |

> Tabla 1.Tipos de usuarios, Code Robot (2022).

* * *

## 5. Panel de administración ##

El siguiente panel administrativo permite al usuario administrador tener una forma rápida de acceso a los módulos de consulta y administración de la plataforma. Para ingresar a este apartado el usuario deberá dar clic en “Panel de perfil” en el menú que se localiza en el header de la plataforma.

![Screenshot](images/admin-web-guide/image29.png)

![Screenshot](images/admin-web-guide/image54.png)
> Figura 14. Panel administrativo, Code Robot (2022).

### 5.1 Archivos ###


Este apartado gestiona el listado de archivos que son subidos a la plataforma por los usuarios, estos pueden ser desde imágenes de productos, logotipos de los negocios, etc.

Al acceder a este módulo la plataforma desplegará una pantalla donde se mostrará el listado de archivos subidos por todos los usuarios, este solo puede ser consultado por el administrador, se distribuye de la siguiente manera:

![Screenshot](images/admin-web-guide/image16.png)
> Figura 15. Gestión de archivos, Code Robot (2022).

1.  Refrescar lista: Permite al administrador actualizar la lista de archivos subidos.
2.  Paginador: Permite navegar entre el listado de archivos.

### 5.2 Bloques ###

Este módulo permite al administrador modificar elementos de la estructura de ciertos elementos del home, por ejemplo elementos que se encuentran en el footer.

Al acceder a este módulo la plataforma desplegará una pantalla donde se mostrará el listado de bloques existentes, los cuales se podrán editar por el administrador, se distribuye de la siguiente manera:

![Screenshot](images/admin-web-guide/image69.png)
> Figura 16. Gestión de bloques, Code Robot (2022).

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por Clave de uso.  
    c.  Al hacer clic en el botón buscar  se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Refrescar lista: Permite al administrador actualizar la lista de bloques existentes.
3.  Editar: Permite al administrador acceder al modo edición del bloque.
4.  Paginador: Permite navegar entre el listado de bloques.

Por otra parte, la plataforma también permite al administrador el acceso rápido para la edición de bloques, con tan solo colocar el cursor encima del bloque aparecerá un engrane para editar.

![Screenshot](images/admin-web-guide/image31.png)
> Figura 17. Acceso rápido para gestión de bloques, Code Robot (2022).

### 5.3 Banner ###

Este módulo permite al administrador editar y crear nuevos banners los cuales serán mostrados en distintas ubicaciones de la plataforma web y móvil, estos tienen la intención de ser anuncios de productos de temporada, informativos del mercado, etc.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de banners existentes, los cuales se podrán editar por el administrador, se distribuye de la siguiente manera:

![Screenshot](images/admin-web-guide/image61.png)

> Figura 18. Gestión de banners, Code Robot (2022).

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por título.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo banner: Permite al administrador acceder al formulario de creación de banner.
3.  Editar: Permite al administrador acceder a la edición de banner existentes.
4.  Paginador: Permite navegar entre el listado de bloques.
5.  Campos de creación o edición: Permite al administrador añadir la información correspondiente en los distintos campos.
6.  Activado: Permite activar o desactivar el banner sin necesidad de borrar el contenido.
7.  Peso: Hace referencia a la posición en la que se mostrará en el carrusel de banners.
8.  Añadir archivo: Permite seleccionar y adjuntar la imagen correspondiente.
9.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar el nuevo banner.

![Screenshot](images/admin-web-guide/image66.png)
> Figura 19. Formulario creación de banners, Code Robot (2022).

### 5.4 Página básica ###

Este apartado es una herramienta que permite al administrador la creación de páginas básicas, las cuales podrán ser utilizadas como páginas informativas, por ejemplo, avisos de privacidad, términos y condiciones, preguntas frecuentes, etc.

Al acceder desde el panel de administración al módulo de “Página básica” el sistema desplegará una pantalla donde se mostrará el listado de páginas existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por título.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nueva página básica: Permite al administrador acceder al formulario de creación de página básica.
3.  Editar: Permite al administrador acceder a la edición de página básica.
4.  Paginador: Permite navegar entre el listado de páginas básicas.

![Screenshot](images/admin-web-guide/image7.png)
> Figura 20. Gestión de páginas básicas, Code Robot (2022).

&ensp;5\.  Alias URL: Permite al administrador brindar un alias para poder generar un link específico.
&emsp;*Ej. Alias Url = “politicas-de-privacidad”*  
&emsp;*En algún bloque o página puede agregar un link siguiendo la siguiente estructura __”/basic-page/p/\[alias URL\]”__*  
&emsp;`<a href=”/basic-page/p/politicas-de-privacidad”> Políticas de privacidad</a>`  

&ensp;6\.  Activado: Permite activar o desactivar el banner sin necesidad de borrar el contenido.  
&ensp;7\.  Añadir archivo: Permite seleccionar y adjuntar la imagen correspondiente.  
&ensp;&ensp;a\.  Permite al usuario utilizar la imagen, se da clic en copiar, se envía al portapapeles y al momento de pegarlo en el campo de contenido en “fuente HTML” se incluirá la imagen con la tag y ruta correspondiente.  
&ensp;8\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar la nueva página.

![Screenshot](images/admin-web-guide/image35.png)
> Figura 21. Formulario de captura de páginas básicas, Code Robot (2022).

### 5.5 Categoría de tienda ###

Este apartado es una herramienta que permite al administrador complementar el catálogo de categorías de tienda a través de la captura o edición de categorías de tiendas, por ejemplo abarrotes, agencia de viajes, comida, artículos de piel, etc.

Estas categorías servirán a los vendedores para seleccionar el giro o ramo industrial al que pertenezca su negocio, las cuales, aparecen en el formulario de captura de una tienda.

Al acceder desde el panel de administración al módulo de “Categoría de tienda” el sistema desplegará una pantalla donde se mostrará el listado de categorías existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nueva categoría de tienda: Permite al administrador acceder al formulario de creación de categoría de tienda.
3.  Editar: Permite al administrador acceder a la edición de categoría de tienda existente.
4.  Paginador: Permite navegar entre el listado de categorías.

![Screenshot](images/admin-web-guide/image64.png)
> Figura 22. Gestión de categoría de tienda, Code Robot (2022).

&ensp;5\.  Peso: Hace referencia a la posición en la que se mostrará en el listado.  
&ensp;6\.  Activado: Permite activar o desactivar la categoría sin necesidad de borrar el contenido.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar la nueva página.  

![Screenshot](images/admin-web-guide/image12.png)
> Figura 23. Formulario de captura de categoría de tienda, Code Robot (2022).

### 5.6 Categoría de producto ###

Este apartado es una herramienta que permite al administrador complementar el catálogo de categorías de productos a través de la captura o edición de categorías de productos, por ejemplo pescado, comida, carnes, aves, lácteos, etc. Hay que tomar en cuenta que estas categorías tienen que ser agrupaciones globales de productos.

Este nivel de categorías permitirá a los vendedores agrupar sus productos, dichas categorías las  podrán utilizar en el formulario de captura de producto.

Al acceder desde el panel de administración al módulo de “Categoría de producto” el sistema desplegará una pantalla donde se mostrará el listado de categorías existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nueva categoría de producto: Permite al administrador acceder al formulario de creación de categoría de producto.
3.  Editar: Permite al administrador acceder a la edición de categoría de producto existente.
4.  Paginador: Permite navegar entre el listado de categorías.

![Screenshot](images/admin-web-guide/image60.png)
> Figura 24. Gestión de categoría de producto, Code Robot (2022).

&ensp;5\.  Peso: Hace referencia a la posición en la que se mostrará en el listado.  
&ensp;6\.  Activado: Permite activar o desactivar la categoría sin necesidad de borrar el contenido.  
&ensp;7\.  Añadir archivos: Permite seleccionar y adjuntar la imagen correspondiente.  
&ensp;8\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar la nueva categoría.  

![Screenshot](images/admin-web-guide/image21.png)
> Figura 25. Formulario de captura de categoría de producto, Code Robot (2022).

### 5.7 Categoría de unidad ###

Este apartado es una herramienta que permite al administrador capturar o editar unidades de medida que serán utilizadas para los productos, por ejemplo pieza, litro, metro, etc.

Al acceder desde el panel de administración al módulo de “Categoría de unidad” el sistema desplegará una pantalla donde se mostrará el listado de categorías existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nueva categoría de unidad: Permite al administrador acceder al formulario de creación de categoría de unidad.
3.  Editar: Permite al administrador acceder a la edición de categoría de unidad existente.
4.  Paginador: Permite navegar entre el listado de categorías.

![Screenshot](images/admin-web-guide/image32.png)
> Figura 26. Gestión de categoría de unidad, Code Robot (2022).

&ensp;5\.  Peso: Hace referencia a la posición en la que se mostrará en el listado.  
&ensp;6\.  Activado: Permite activar o desactivar la categoría sin necesidad de borrar el contenido.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar la nueva categoría.  

![Screenshot](images/admin-web-guide/image34.png)
> Figura 27. Formulario de captura de categoría de unidad, Code Robot (2022).

### 5.8 Estado ###

Este módulo permite al administrador editar y crear nuevos Estados de la República, actualmente la plataforma tiene capturados todos los Estados para su posterior utilización en otras Entidades Federativas.

Este apartado será utilizado por el usuario comprador al capturar alguna dirección de envío de sus pedidos dentro de su cuenta.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de Estados, los cuales se podrán editar por el administrador, se distribuye de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo estado: Permite al administrador acceder al formulario de creación del nuevo Estado.
3.  Paginador: Permite navegar entre el listado de categorías.

![Screenshot](images/admin-web-guide/image41.png)
> Figura 28. Gestión de Estados de la República, Code Robot (2022).

&ensp;4\.  ID oficial: Hace referencia al número identificador oficial de cada Entidad Federativa.  
&ensp;5\.  Abreviación: Hace referencia a la abreviación del nombre del Estado.  
&ensp;6\.  Activado: Permite activar o desactivar el Estado sin necesidad de borrar el contenido.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar el nuevo Estado.  

![Screenshot](images/admin-web-guide/image51.png)
> Figura 29. Formulario de captura de Estados de la República, Code Robot (2022).

### 5.9 Municipio ###

Este módulo permite al administrador editar y añadir nuevos municipios de los Estados de la República.

Este apartado será utilizado por el usuario comprador al capturar alguna dirección de envío de sus pedidos dentro de su cuenta.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de municipios existentes, los cuales se podrán editar por el administrador, se distribuye de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo municipio: Permite al administrador acceder al formulario de creación de nuevo municipio.
3.  Editar: Permite al administrador acceder a la edición de un municipio existente.
4.  Paginador: Permite navegar entre el listado de categorías.

![Screenshot](images/admin-web-guide/image56.png)
> Figura 30. Gestión de municipios, Code Robot (2022).

&ensp;5\.  ID oficial: Hace referencia al número identificador oficial de cada municipio.  
&ensp;6\.  Activado: Permite activar o desactivar el municipio sin necesidad de borrar el contenido.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar el nuevo municipio.  

![Screenshot](images/admin-web-guide/image19.png)
> Figura 31. Formulario de captura de municipios, Code Robot (2022).

### 5.10 Métodos de pago ###

Este módulo permite administrar y consultar los métodos de pago disponibles en la plataforma. Dicho apartado podrá ser usado por el vendedor al seleccionar los métodos de pago aceptados en su tienda.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de métodos de pago, los cuales se podrán editar por el administrador, se distribuye de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por método de pago.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Refrescar lista: Permite al administrador actualizar la lista de métodos de pago existentes.
3.  Editar: Permite al administrador acceder a la edición de un método de pago.
4.  Paginador: Permite navegar entre el listado de métodos de pago.

![Screenshot](images/admin-web-guide/image15.png)
> Figura 32. Gestión de métodos de pago, Code Robot (2022).

&ensp;5\.  Activado: Permite activar o desactivar el método de pago sin necesidad de borrar el contenido.  
&ensp;6\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar el nuevo método de pago.  

![Screenshot](images/admin-web-guide/image5.jpg)
> Figura 33. Formulario de edición de métodos de pago, Code Robot (2022).

Este apartado requiere la interacción con el equipo técnico, ya que no deberá activar una nueva opción de pago a menos que sea autorizada por el área de desarrollo.

### 5.11 Impuestos ###

Este apartado es una herramienta que permite al administrador crear o editar impuestos los cuales podrán ser utilizados para la venta de productos, un ejemplo de ellos puede ser el IVA, así como, realizar cálculo de tarifas en los montos a pagar en el carrito de compra.

Al acceder desde el panel de administración al módulo de “Impuestos” el sistema desplegará una pantalla donde se mostrará el listado de impuestos existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo impuesto: Permite al administrador acceder al formulario de creación de nuevo impuesto.
3.  Editar: Permite al administrador acceder a la edición del impuesto existente.
4.  Paginador: Permite navegar entre el listado de impuestos.

![Screenshot](images/admin-web-guide/image74.png)
> Figura 34. Gestión de impuestos, Code Robot (2022).

&ensp;5\.  Tasa: Hace referencia a un tributo que debe pagar el consumidor o usuario, este se captura en números enteros, ejemplo el IVA (16% = 16).  
&ensp;6\.  Activado: Permite activar o desactivar el impuesto sin necesidad de borrar el contenido.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar el nuevo impuesto.  

![Screenshot](images/admin-web-guide/image9.png)
> Figura 35. Formulario de creación de impuestos, Code Robot (2022).

### 5.12 Mercado ###

Este apartado es una herramienta que permite al administrador editar y dar de alta nuevos mercados de la ciudad. Este apartado será utilizado cuando se inicie con la implementación de la plataforma en distintos mercados.

Al acceder desde el panel de administración al módulo de “Mercados” el sistema desplegará una pantalla donde se mostrará el listado de mercados existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre del mercado.  
    c.  Dirección.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo mercado: Permite al administrador acceder al formulario de creación de nuevo mercado.
3.  Editar: Permite al administrador acceder a la edición del mercado existente.
4.  Paginador: Permite navegar entre el listado de mercados.

![Screenshot](images/admin-web-guide/image38.png)
> Figura 36. Gestión de mercados, Code Robot (2022).

![Screenshot](images/admin-web-guide/image25.png)
> Figura 37. Formulario de creación de mercado 1, Code Robot (2022).

![Screenshot](images/admin-web-guide/image37.png)
> Figura 38. Formulario de creación de mercado 2, Code Robot (2022).

&ensp;5\.  Herramientas delimitadoras de polígonos: Permite al usuario delimitar manualmente un polígono a través de puntos el área de coberturas de envío del mercado así como editar y borrar.  
&ensp;6\.  Área de cobertura: El área verde hace referencia a la zona de cobertura de envíos.  
&ensp;7\.  Código de seguridad: El administrador deberá asignar un código de seguridad para el mercado, el cual, será proporcionado posteriormente a los locatarios del mercado para que puedan dar de alta su negocio.  
&ensp;8\.  Activado: Permite activar o desactivar el mercado sin necesidad de borrar el contenido.  
&ensp;9\.  Añadir archivos: Permite seleccionar y adjuntar la imagen correspondiente.  
&ensp;10\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar el nuevo mercado.

### 5.13 Tiendas ###

Este módulo permite al administrador editar y añadir nuevas tiendas, así como, consultar productos, órdenes y preguntas.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de tiendas capturadas, las cuales se podrán editar por el administrador, se distribuye de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre de la tienda.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nueva tienda: Permite al administrador acceder al formulario de creación de nueva tienda.
3.  Paginador: Permite navegar entre el listado de tiendas.
4.  Productos: Permite consultar el listado de productos capturados por la tienda.
5.  Órdenes: Permite consultar el historial de órdenes realizadas en esa tienda.
6.  Preguntas: Permite consultar el historial de preguntas realizadas por los usuarios.

![Screenshot](images/admin-web-guide/image50.png)
> Figura 39. Gestión de tiendas, Code Robot (2022).

![Screenshot](images/admin-web-guide/image10.png)
> Figura 40. Formulario de creación de tienda, Code Robot (2022).

&ensp;7\.  Métodos de entrega: El administrador o el vendedor al capturar su negocio deberá seleccionar sus métodos de entrega, ya sea a domicilio o pickup. Es importante considerar que este campo es condicional, al seleccionar envío a domicilio se desprenden consideraciones como:  
    &ensp;&ensp;a\.  "Permitir asignar envíos a repartidores proporcionados por el mercado". Permite a todos los repartidores ver cuando solicitas un envío a domicilio.  
    &ensp;&ensp;b\.  "Repartidores asignados". Solo estos repartidores asignados podrán ver cuando solicites un envío a domicilio (Repartidores exclusivos).  
    &ensp;&ensp;c\.  "“Políticas de envío", notificar si se tiene una consideración especial, ej: envíos de 7 am a 2 pm, denotando tu rango de operación de este tipo.  
&ensp;8\.  Activado: Permite activar o desactivar la tienda sin necesidad de borrar el contenido.

![Screenshot](images/admin-web-guide/image20.png)
> Figura 41. Formulario de creación de tienda 2 , Code Robot (2022).

&ensp;9\.  Formas de pago: Permite al usuario seleccionar si su negocio acepta efectivo, el uso de alguna pasarela de pago disponible para pagos en línea con tarjeta o ambas opciones.  
&ensp;10\.  Terminal punto de venta: El usuario que cuente en su local con terminal de stripe o alguna otra deberá seleccionar este apartado.  
&ensp;11\.  Añadir archivos: Permite seleccionar y adjuntar la imagen correspondiente.  
&ensp;12\.  Giro/ramo industrial: Hace referencia a giro del negocio, por ejemplo Alimentos, servicios, etc.  

![Screenshot](images/admin-web-guide/image8.png)
> Figura 42. Formulario de creación de tienda 3 , Code Robot (2022).

&ensp;13\.  Bloquear entrada de nuevas órdenes: Permite al vendedor cerrar la opción de que sus productos puedan ser añadidos al carrito de compra.  
&ensp;14\.  Configurar horario de atención: Permite calendarizar el horario de atención del negocio.  
&ensp;15\.  Configurar horario de envío a domicilio: Permite al usuario añadir horario específico de envío de productos a domicilio.  
&ensp;16\.  Guardar: Al finalizar el llenado de los campos el administrador podrá guardar la nueva tienda.  

### 5.14 Tienda favorita ###

Este módulo permite al administrador consultar el listado de tiendas que han sido seleccionadas como favoritas por los usuarios, así como, el permiso para eliminar las tiendas favoritas del listado de los usuarios.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de las tiendas favoritas, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por perfil del comprador.  
    c.  Búsqueda por tienda.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Refrescar lista: Permite al administrador actualizar la lista de tiendas favoritas existentes.
3.  Paginador: Permite navegar entre el listado de tiendas favoritas.

![Screenshot](images/admin-web-guide/image2.png)
> Figura 43. Gestión de tiendas favoritas, Code Robot (2022).

### 5.15 Productos ###

Este apartado es una herramienta que permite al administrador y vendedores editar y dar de alta nuevos productos.

Al acceder desde el panel de administración al módulo de “Productos” el sistema desplegará una pantalla donde se mostrará el listado de productos existentes, los cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo producto: Permite al administrador acceder al formulario de creación de nuevo producto.
3.  Paginador: Permite navegar entre el listado de productos.

![Screenshot](images/admin-web-guide/image67.png)
> Figura 44. Gestión de productos, Code Robot (2022).

![Screenshot](images/admin-web-guide/image26.png)
> Figura 45. Formulario de captura de productos 1, Code Robot (2022).

&ensp;4\.  Showroom: seleccionar el check si lo capturado corresponde a un servicio el cual no cuente con un precio fijo, a continuación se muestran las siguientes figuras de ejemplo:

&ensp;&ensp;a\.  Producto normal con precio:  
    ![Screenshot](images/admin-web-guide/image48.png)  
   > Figura 46. Ejemplo de producto normal, Code Robot (2022).

&ensp;&ensp;b\.  Servicio o producto con precio variable:  
    ![Screenshot](images/admin-web-guide/image28.png)
   > Figura 47. Ejemplo servicio o producto, Code Robot (2022).

![Screenshot](images/admin-web-guide/image36.png)
> Figura 48. Formulario de captura de productos 2, Code Robot (2022).

&ensp;5\.  Variantes y componentes: Permite añadir variantes del mismo producto que no afecten al precio, por ejemplo, chilaquiles; Categoría  = “Sabor”, Variante(s) = “rojos, verdes”.  
&ensp;6\.  Añadir archivos: Permite seleccionar y adjuntar la imagen correspondiente.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador o vendedor podrá guardar el nuevo producto.  

### 5.16 Carga masiva de productos ###

Este apartado es una herramienta que permite al administrador y vendedores editar y dar de alta nuevos productos de manera masiva, a través de archivos csv con formato específico.

Al acceder desde el panel de administración al módulo de “Carga masiva de productos” el sistema desplegará una pantalla donde se mostrará el listado de importaciones masivas anteriores, y por otra parte el formulario para crear importación, el cual se muestra de la siguiente manera:

1.  Tipo: Permite seleccionar el tipo de categoría que será, en este caso productos.
2.  Juego de caracteres: Al seleccionar el juego de caracteres a utilizar el sistema pone a disposición una plantilla y un diccionario de datos, de manera que puedas generar el archivo que se solicitará en el punto 3.
3.  Añadir archivos: Permite seleccionar y adjuntar el archivo csv correspondiente.

![Screenshot](images/admin-web-guide/image17.png)
> Figura 49. Carga masiva, Code Robot (2022).

### 5.17 Tracking de producto ###

Este apartado es una herramienta que permite al administrador consultar y eliminar el tracking de los productos que han sido consultados por los usuarios en la plataforma. Este apartado será mayormente utilizado para consulta.

Al acceder desde el panel de administración al módulo de “Tracking de productos” el sistema desplegará una pantalla donde se mostrará el listado de todos los productos que han sido pedidos, evaluados, etc. los cuales se muestran de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por producto.  
    c.  Búsqueda por usuario.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Eliminar: Permite al administrador eliminar el producto en tracking.
3.  Paginador: Permite navegar entre el listado de tracking de productos.

![Screenshot](images/admin-web-guide/image52.png)
> Figura 50. Gestión de tracking de productos, Code Robot (2022).

### 5.18 Precios ###

Este módulo permite al administrador consultar y capturar nuevos precios de los productos.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de las tiendas precios existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por producto.  
    c.  Búsqueda por tienda.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo precio: Permite al administrador capturar nuevos precios de productos.
3.  Paginador: Permite navegar entre el listado de precios.

![Screenshot](images/admin-web-guide/image68.png)
> Figura 51. Gestión de precios, Code Robot (2022).

&ensp;4\.  Producto: Permite seleccionar el producto al cual se le asignará precio.  
&ensp;5\.  Tienda: Permite al usuario seleccionar la tienda correspondiente al producto.  
&ensp;6\.  Guardar: Al finalizar el llenado de los campos el administrador o vendedor podrá guardar el nuevo precio, tomar en cuenta que el último precio asignado será el que se visualizará al usuario final.  

![Screenshot](images/admin-web-guide/image62.png)
> Figura 52. Formulario de captura de precios, Code Robot (2022).

### 5.19 Orden ###

Este módulo permite al administrador consultar el historial de órdenes que han sido creadas por los usuarios.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de órdenes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por cliente.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Elemento de orden: Permite al administrador acceder a un listado administrativo de los productos solicitados de la orden.
3.  Detalle de orden: Permite consultar a detalle el pedido completo, como sus productos solicitados, método de entrega, etc.
4.  Paginador: Permite navegar entre el listado de órdenes.

![Screenshot](images/admin-web-guide/image18.png)
> Figura 53. Gestión de órdenes, Code Robot (2022).

&ensp;5\.  Elemento de orden: Al acceder al elemento de orden el sistema mostrará un listado administrativo el cual también cuenta  con un filtro de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    &ensp;&ensp;a\.  Búsqueda por ID.  

![Screenshot](images/admin-web-guide/image73.png)
> Figura 54. Detalle de orden, Code Robot (2022).

### 5.20 Calificación de orden ###

Este apartado es una herramienta que permite al administrador consultar las calificaciones que los usuarios dejan después de finalizar una compra. Este apartado será mayormente utilizado para consulta.

Al acceder desde el panel de administración al módulo de “Calificación de órdenes” el sistema desplegará una pantalla donde se mostrará el listado de todas las órdenes que han sido calificadas, las cuales se muestran de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por orden.  
    c.  Búsqueda por comprador.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Editar: Permite al administrador editar / consultar la calificación.
3.  Paginador: Permite navegar entre el listado de calificaciones.

![Screenshot](images/admin-web-guide/image59.png)
> Figura 55. Gestión de calificaciones de orden, Code Robot (2022).

### 5.21 Preguntas y respuestas ###

Este apartado es una herramienta que permite al administrador consultar, editar y eliminar las preguntas que los usuarios hacen sobre los productos o servicios.

Al acceder desde el panel de administración al módulo de “Preguntas y respuestas” el sistema desplegará una pantalla donde se mostrará el listado de todas las preguntas que han generado los usuarios, las cuales se muestran de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por tipo de pregunta.  
    c.  Búsqueda por usuario.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Editar: Permite al administrador editar / consultar las preguntas.
3.  Paginador: Permite navegar entre el listado de preguntas y respuestas.

![Screenshot](images/admin-web-guide/image53.png)
> Figura 56. Gestión de preguntas y respuestas, Code Robot (2022).

### 5.22 Chat ###

Este apartado es una herramienta que permite al administrador consultar, editar y eliminar chats entre el vendedor y comprador.

Al acceder desde el panel de administración al módulo de “Chat” el sistema desplegará una pantalla donde se mostrará el listado de todos los chats que han sido creados al existir una orden o pedido de por medio, los cuales se muestran de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por asunto.  
    c.  Búsqueda por clave de uso.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Ver mensajes: Permite ver a detalle la conversación.
3.  Editar: Permite al administrador editar / consultar los chats.
4.  Paginador: Permite navegar entre el listado de chats.

![Screenshot](images/admin-web-guide/image13.png)
> Figura 57. Gestión de chat, Code Robot (2022).

### 5.23 Domicilios ###

Este módulo permite al administrador consultar el listado de domicilios creados en la plataforma por parte de los usuarios.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de domicilios, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por login cliente.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo domicilio: Permite acceso al formulario de creación de domicilio.
3.  Paginador: Permite navegar entre el listado de domicilios.

![Screenshot](images/admin-web-guide/image1.png)
> Figura 58. Gestión de domicilios, Code Robot (2022).

![Screenshot](images/admin-web-guide/image44.png)
> Figura 59. Formulario de captura de domicilio 1, Code Robot (2022).

![Screenshot](images/admin-web-guide/image70.png)
> Figura 60. Formulario de captura de domicilio 2, Code Robot (2022).

&ensp;4\.  Guardar: Al finalizar el llenado de los campos el administrador o vendedor podrá guardar el nuevo domicilio.

### 5.24 Notificaciones ###

Este módulo permite al administrador consultar el historial de notificaciones lanzadas por la plataforma.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de notificaciones, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por tipo de notificación.  
    c.  Búsqueda por subtipo.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Editar: Permite acceder a una visualización más detallada de la notificación.
3.  Paginador: Permite navegar entre el listado de notificaciones.

![Screenshot](images/admin-web-guide/image55.png)
> Figura 61. Gestión de notificaciones, Code Robot (2022).

### 5.25 Datos de configuración ###

Este apartado es una herramienta que permite al administrador consultar y eliminar datos de configuración. Este apartado será mayormente utilizado para consulta.

Al acceder desde el panel de administración al módulo de “Datos de configuración” el sistema desplegará una pantalla donde se mostrará el listado de todos los datos que han sido capturados, los cuales se muestran de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por clave.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Editar: Permite acceder a una visualización más detallada del dato.
3.  Paginador: Permite navegar entre el listado de datos.

![Screenshot](images/admin-web-guide/image40.png)
> Figura 62. Gestión de datos de configuración, Code Robot (2022).

### 5.26 Formulario de contacto ###

Este apartado es una herramienta que permite al administrador generar formularios simples de contacto, donde los usuarios podrán notificar sobre algún tema al administrador de plataforma.

![Screenshot](images/admin-web-guide/image46.png)
> Figura 63. Formulario de contacto, Code Robot (2022).

### 5.27 Reportes ###

Este apartado es una herramienta que permite al administrador visualizar de manera general datos sobre el comportamiento de los usuarios con respecto al uso de la plataforma, permitiendo observar las preferencias sobre métodos de envío o formas de pago ya sea globales o por tienda.

![Screenshot](images/admin-web-guide/image11.png)
> Figura 64. Reportes, Code Robot (2022).

### 5.28 Llaves de pago ###

Este apartado es una herramienta que permite al administrador vincular la plataforma Mi Mercado AMG con la ApiKey o llave de pago desprendida del uso de alguna pasarela de pago, la plataforma actualmente cuenta con la integración de la pasarela de pagos de Billpocket, esto con el fin de recibir pagos en línea. Al ingresar al apartado el sistema desplegará el siguiente formulario:

![Screenshot](images/admin-web-guide/image33.png)
> Figura 65. Llaves de pago, Code Robot (2022).

1.  Clave: Esta clave será proporcionada por la pasarela de pagos billpocket, la cual se denomina “APIKEY”
2.  Usuario: El usuario deberá añadir su código o nombre de usuario.
3.  Método de pago: Para pago en línea se deberá seleccionar “Billpocket”, este apartado está relacionado al módulo de métodos de pago (5.10 Métodos de pago) mencionado anteriormente.
4.  Guardar: Al finalizar el llenado de los campos el usuario tendrá vinculada su llave de pago.

### 5.29 Registro de pasarela ###

Este apartado es una interfaz administrativa enfocada exclusivamente al registro de cuentas para la pasarela de pago “Billpocket”, la cual es un servicio de comercio electrónico con el que se autorizan pagos a negocios de sus ventas en línea. Este apartado cuenta con distintas pestañas que se detallan a continuación:

![Screenshot](images/admin-web-guide/image6.png)

> Figura 66. Pasarela de pago, Code Robot (2022).

1.  Registro en Billpocket: Al dar clic en el botón de registrarse en Billpocket, el sistema mostrará el formulario donde el usuario deberá capturar todos sus datos para la creación de su cuenta.
2.  Completar registro: Una vez que el usuario tenga creada su cuenta el usuario deberá completar su información dentro de la plataforma de Billpocket. Se podrá acceder desde el siguiente enlace: https://dashboard.billpocket.com/panel/login.
3.  Almacenar API Key: Este apartado permite vincular directamente la creación de una llave de pago de tipo Billpocket, de igual manera muestra las llaves de pago guardadas, mostradas por ID, usuario, método de pago y número de ApiKey.
4.  Listado de tiendas: Una vez se tienen dadas de alta las llaves de pago permite al administrador seleccionar la tienda a consultar y definir si para la tienda en cuestión desea agregar alguna de las llaves previamente capturadas o remover la llave asignada a la tienda.

### 5.30 Alertas ###

Este módulo permite al usuario la gestión y creación de alertas, estas alertas son mensajes que se muestran en distintas zonas, por ejemplo en el home el mensaje de medidas sanitarias contra el COVID-19.

Al acceder a este módulo administrativo la plataforma desplegará una pantalla donde se mostrará el listado de alertas, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por asunto.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nueva alerta: Permite acceso al formulario de creación de nueva alerta.
3.  Paginador: Permite navegar entre el listado de domicilios.

![Screenshot](images/admin-web-guide/image22.jpg)
> Figura 67. Listado de alertas, Code Robot (2022).

&ensp;4\.  Clase de alerta: El usuario podrá definir que tipo de alerta es, estas pueden ser de satisfacción, error, advertencia o actualización.  
&ensp;&ensp;a\.  Al definir una alerta de tipo actualización será necesario introducir la versión nueva de la aplicación, ya que esta es cotejada para definir si es necesario o no mostrarla al usuario.  
&ensp;5\.  Destino de alerta: Permite seleccionar la ubicación de la alerta, por ejemplo, home.  
&ensp;6\.  Peso: Hace referencia a la posición en la que se mostrará, esto siendo el caso en que se muestre en el mismo destino.  
&ensp;7\.  Guardar: Al finalizar el llenado de los campos el usuario podrá guardar la nueva alerta.  

![Screenshot](images/admin-web-guide/image14.png)
> Figura 68. Formulario de creación de alerta, Code Robot (2022).


### 5.31 Detalles de transacción ######

Este apartado es una herramienta que permite al administrador visualizar de manera general datos sobre el comportamiento de las transacciones realizadas con respecto al uso de la pasarela de pagos, esta interfaz es exclusiva para la pasarela de pagos de Billpocket, permitiendo observar el ID, petición, respuesta, ID de orden, estatus, dispositivo donde se realizó, tipo, tienda, cliente y método de pago.

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Refrescar lista: Permite al usuario actualizar la lista de transacciones.

![Screenshot](images/admin-web-guide/image57.png)
> Figura 69. Formulario de creación de alerta, Code Robot (2022).

* * *

## 6\. Administración ##


### 6.1 Indexado de tiendas e indexado de productos ######

El indexado de tiendas y productos es una herramienta que se encuentra en el apartado de administración, los cuales, deberán ser utilizados al levantar alguna instancia, o cuando el administrador realice cambios en a tiendas y/o productos.

Es importante ya que de esta indexación depende el que se muestren correctamente los datos del apartado de búsqueda.

![Screenshot](images/admin-web-guide/image4.png)
> Figura 70. Indexados, Code Robot (2022).

* * *

## 7\. Consideraciones generales ##

Con la finalidad de dar cierre al presente manual administrativo y dada la responsabilidad de uso de algunos de los módulos, a continuación se describen distintas responsabilidades que el administrador deberá tener presente en todo momento:

1.  Asignación de roles de usuario: Se considera importante la asignación de roles de usuario, especialmente al capturar roles de tipo “Repartidor / delivery”, ya que el administrador en todo momento será el responsable de la activación de este tipo de cuentas.
2.  Código de seguridad del mercado: Al levantar la instancia de nuevo mercado, el administrador será el responsable del código de mercado, el cual toma gran relevancia para los locatarios, sin el código no se puede registrar ninguna tienda. Siempre tenerlo en cuenta para ser proporcionado únicamente a los locatarios.
3.  Actualizar el indexado de tiendas y productos.
4.  Áreas de envíos: Conllevan la responsabilidad de delimitar y capturar el polígono de área de cobertura por mercado.
5.  Notificaciones: Estar al pendiente de la relevancia de las notificaciones a mostrar a los usuarios, como pueden ser la notificación de recomendaciones por Covid19 o las notificaciones por actualización de la aplicación.
6.  Pasarela de pagos: Estar en constante comunicación y brindar apoyo a los locatarios que deseen contar con la alternativa de pagos en línea para completar su perfil y generar y configurar las llaves (apikey) de los locales.
7.  Formularios de contacto: El administrador tiene la responsabilidad de resolver dudas y estar atento en todo momento a las dudas de los formularios activos.
8.  Alimentación de la página de ayuda: Se recomienda que inicialmente se vaya recabando una base de preguntas frecuentes realizada por los locatarios y usuarios, esto para posteriormente añadir ese listado de preguntas a la sección de “Ayuda”, la cual aparece en “Mi cuenta” dentro de la app.
9.  Actualizar información de consulta:  
    a.  Términos de uso  
    b.  Políticas de privacidad

__*Fecha de actualización: 21-04-2022*__