# Instalación #
## 1. Introducción ##


### 1.1 Alcance de este documento ###

El propósito de este manual es el de facilitar a los usuarios técnicos, el despliegue, instalación y migración de una instancia de Mi Mercado AMG.

Además de ofrecer a dichos usuarios la información de contexto en cuanto a las tecnologías utilizadas y las especificaciones técnicas que deberán de cumplir para garantizar el adecuado funcionamiento del sistema. Para conocer más detalles de la estructura interna de la solución revisar el documento de arquitectura de la solución.

### 1.2 Cómo utilizar este manual ######

Este manual ha sido preparado como un documento de referencia para los encargados técnicos del Mi Mercado AMG. La información que aquí se presenta está orientada a informar al usuario técnico en cuanto a los procedimientos que deberá de seguir para generar una instancia del aplicativo y para mantenerla en funcionamiento.

Para su utilización consulte la tabla de contenidos anterior y siga las instrucciones cuidadosamente.

* * *

## 2\. Generalidades del sistema ##

### 2.1 Requerimientos mínimos ###

A continuación se citan los requerimientos necesarios para la instalación de la solución en un entorno productivo, sin embargo debido a la naturaleza de la solución se citan como recomendación y podrán ser reemplazadas por tecnologías con capacidades o características similares.

#### 2.1.1 Servidor ####

Sistema Operativo: Ubuntu / Última versión.  
Memoria RAM: 16 GB  
Disco Duro: 160 GB mínimo
Cuenta SSH: Con privilegios root para la instalación de librerías, dependencias y aplicativos
Procesador: 4 núcleos
Puertos: 80, 443  
  
<br>
De manera inicial se plantea como un servidor único para albergar los diversos componentes de la solución, se marca como punto de entrada para facilitar la adopción de la misma, la posterior segmentación de servicios en servidores independientes depende de la madurez del área receptora y de sus criterios técnicos operacionales.

#### 2.1.2 Dominio ####

Se requiere de la definición de un dominio para el acceso a la plataforma web.  

#### 2.1.3 Certificado HTTPS ####

Se requiere la generación de un certificado HTTPS para el servidor entregado, este es un requisito fundamental para el consumo de servicios desde aplicaciones móviles de forma segura.

#### 2.1.4 Cuenta SMTP ####

El aplicativo notifica diversos eventos a los usuarios a través de correo electrónico, el servicio requiere de una cuenta SMTP para la salida de los mismos.

#### 2.1.5 Servicios de terceros ####

##### 2.1.5.1 Cuentas de desarrollador de Google Play Store y de Apple App Store #####

Este servicio requiere un registro y una membresía:

Costo al 04/11/21 (Play Store): $25.00 dólares /pago único

Costo al 04/11/21 (App Store): $99.00 o $299 (versión Enterprise) dólares / pago anual

[https://play.google.com/console/u/0/signup](https://play.google.com/console/u/0/signup)  
[https://developer.apple.com/es/support/enrollment/](https://developer.apple.com/es/support/enrollment)  

<br>

##### 2.1.5.2 Google (Firebase Cloud Messaging) #####

Firebase Cloud Messaging (FCM) es una solución de mensajería multiplataforma que permite enviar mensajes de forma segura y gratuita. FCM se utiliza en el proyecto para notificar a una app cliente que un correo electrónico nuevo o que otros datos están disponibles para la sincronización. (Servicio gratuito de Google) | Se requiere un api key para su uso.  
<br>

##### 2.1.5.3 reCAPTCHA V3 #####

reCAPTCHA v3 devuelve una puntuación por cada solicitud del usuario, se utiliza en la solución para identificar transacciones de riesgo (Servicio opcional, se puede apagar y prender), (Servicio gratuito de Google) | Se requiere un api key para su uso.  
<br>

##### 2.1.5.4 Inicio de sesión de terceros (SSO) #####

Se incluyen funcionalidades de inicio de sesión con Google y Apple Sign, (Servicio opcional, se puede apagar y prender), (Servicio gratuito de Google y Apple) | Se requiere un api key para su uso.

  
### 2.2 Herramientas utilizadas para el desarrollo y desplegado ###

#### 2.2.1 JDK 1.11+ ####

Java Development Kit (JDK), es un software que provee herramientas de desarrollo para la creación de programas en Java. Puede instalarse en una computadora local o en una unidad de red.

#### 2.2.2 Tomcat 9+ ####

Tomcat (también llamado Jakarta Tomcat o Apache Tomcat) funciona como un contenedor de servlets desarrollado bajo el proyecto Jakarta en la Apache Software Foundation. Tomcat implementa las especificaciones de los servlets y de JavaServer Pages (JSP) de Sun Microsystems.

#### 2.2.3 MinIo ####

Minio es un servidor de almacenamiento de objetos y archivos.

#### 2.2.4 Elasticsearch ####

Elasticsearch es un motor de analítica y análisis distribuido, gratuito y abierto para todos los tipos de datos, incluidos textuales, numéricos, geoespaciales, estructurados y no estructurados.

#### 2.2.5 Postgresql 12+ ####

PostgreSQL es un sistema de gestión de bases de datos objeto-relacional, distribuido bajo licencia BSD y con su código fuente disponible libremente.

#### 2.2.6 Postgis ####

PostGIS convierte al sistema de administración de bases de datos PostgreSQL en una base de datos geoespacial mediante la adición de tres características: tipos de datos geoespaciales, índices geoespaciales y funciones que operan sobre ellos.

#### 2.2.7 Nginx ####

Nginx ​ es un servidor web/proxy inverso ligero de alto rendimiento y un proxy para protocolos de correo electrónico.​​ Es software libre y de código abierto, licenciado bajo la Licencia BSD simplificada.

#### 2.2.8 Spring Boot / JHipster ####

Spring Boot es un subproyecto de Spring, el mismo busca facilitarnos la creación de proyectos con el framework Spring eliminando la necesidad de crear largos archivos de configuración xml, Spring Boot provee configuraciones por defecto para Spring y otra gran cantidad de librerías. JHipster es un generador de aplicaciones de código abierto que se utiliza para desarrollar rápidamente aplicaciones web modernas utilizando Angular y Spring Framework. El utilizar Spring Framework facilita la documentación en código y provee un marco de desarrollo fácil de entender y extender gracias a la utilización de patrones de diseño probados a través del tiempo.  
A continuación se detalla el stack tecnológico del proyecto:

*   Jhipster generator
*   Spring Boot
*   Maven
*   Spring Security
*   Spring MVC REST + Jackson
*   Spring Data JPA + Bean Validation
*   Spring State Machine
*   Liquibase
*   Angular 10
*   Bootstrap 4

#### 2.2.9 Ionic ####

IONIC es un SDK de frontend de código abierto para desarrollar aplicaciones híbridas basado en tecnologías web (HTML, CSS y JS). Este es un framework que nos permite desarrollar aplicaciones para iOS nativo, Android y la web, desde una única base de código.

#### 2.2.10 IntelliJ IDEA ####

IntelliJ IDEA es un ambiente de desarrollo integrado (IDE) para el desarrollo de programas en Java y Java Web.

#### 2.2.11 Android studio ####

Android Studio es el entorno de desarrollo integrado oficial para la plataforma Android. Fue anunciado el 16 de mayo de 2013 en la conferencia Google I/O, y reemplazó a Eclipse como el IDE oficial para el desarrollo de aplicaciones para Android. La primera versión estable fue publicada en diciembre de 2014.

#### 2.2.12 XCode ####

Xcode es un completo conjunto de herramientas para desarrolladores que permite crear apps para Mac, iPhone, iPad, Apple Watch y Apple TV. Xcode combina las funcionalidades de diseño de interfaz de usuario, programación, pruebas, depuración y envío a App Store en un flujo de trabajo unificado.

### 2.3 Arquitectura de la solución ###

En el siguiente diagrama se detalla a la arquitectura de la solución tecnológica a través de su diagrama de despliegue

![Screenshot](images/installation/image11.png)
> Figura 1. Diagrama de despliegue, Code Robot (2022).

* * *

## 3\. Proceso de instalación ##

En este apartado se detallan los pasos necesarios para realizar el despliegue o instalación de la solución tecnológica.

### 3.1 Instalación y configuración de MinIo ###
  
Ingresar con privilegios root

    sudo bash  

Actualizar repositorios de ubuntu e instalar librerías base:

    apt update  
    apt install apt-transport-https ca-certificates wget gnupg curl git

Ingresar a directorio de usuario

    cd  

Ingresar al directorio de usuario.

    cd /tmp  
	git clone https://gitlab.com/coderobot-giz-mmamg/mi-mercado-webapp-binaries.git  
	cd mi-mercado-webapp-binaries
  
Otorgar permisos de ejecución a binario

    chmod +x minio

Mover el archivo al directorio /usr/local/bin donde el script de inicio systemd de Minio espera encontrarlo
    
    mv minio /usr/local/bin

Por razones de seguridad agregar un usuario específico para la ejecución de minio

    useradd -r minio-user -s /sbin/nologin

Actualizar el ejecutable minio marcando como nuevo propietario a minio-user

    chown minio-user:minio-user /usr/local/bin/minio

A continuación, crear un directorio donde Minio almacenará los archivos

    mkdir /usr/local/share/minio

Otorgar la propiedad de ese directorio a minio-user:

    chown minio-user:minio-user /usr/local/share/minio
  
Preparar carpeta de almacenamiento de archivos de configuración

    mkdir /etc/minio

Otorgar la propiedad de ese directorio a minio-user:

    chown minio-user:minio-user /etc/minio

Usar nano para crear un nuevo archivo de configuración

    nano /etc/default/minio

Con el siguiente contenido:

    MINIO\_ACCESS\_KEY\="mi-mercado-amg-random-key"  
    MINIO\_SECRET\_KEY\="mi-mercado-amg-random-secret-key"  
    MINIO\_VOLUMES="/usr/local/share/minio/"  
    MINIO\_OPTS="-C /etc/minio --address 0.0.0.0:9000"  

Usar nano para crear un nuevo script de inicio de Minio

    nano /etc/systemd/system/minio.service

Con el siguiente contenido:

    [Unit]

        Description=MinIO  
        Documentation=https://docs.min.io  
        Wants=network-online.target  
        After=network-online.target  
        AssertFileIsExecutable=/usr/local/bin/minio  

    [Service]
        WorkingDirectory=/usr/local/  
          
        User=minio-user
        Group=minio-user  
          
        EnvironmentFile=/etc/default/minio
        ExecStartPre=/bin/bash -c "if [ -z \"${MINIO_VOLUMES}\" ]; then echo \"Variable MINIO_VOLUMES not set in /etc/default/minio\"; exit 1; fi"  
          
        ExecStart=/usr/local/bin/minio server $MINIO_OPTS $MINIO_VOLUMES
        
        # Let systemd restart this service always
        Restart=always
        
        # Specifies the maximum file descriptor number that can be opened by this process
        LimitNOFILE=65536
        
        # Disable timeout logic and wait until process is stopped
        TimeoutStopSec=infinity
        SendSIGKILL=no

    [Install]

        WantedBy=multi-user.target
        # Built for ${project.name}-${project.version} (${project.name})

Luego, ejecute el siguiente comando para cargar todas las unidades systemd:
    
    systemctl daemon-reload
    

Finalmente, habilite Minio para que se inicie en el arranque:

    systemctl enable minio  
    systemctl start minio

Utilice los siguientes comandos para iniciar, detener o reiniciar el nuevo servicio

    systemctl start minio  
    systemctl stop minio  
    systemctl restart minio  


### 3.2 Instalación y configuración de ElasticSearch ###

Importar clave de repositorio GPG

    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

Agregar el repositorio de Elasticsearch al sistema a través del siguiente comando

    sudo sh -c 'echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" > /etc/apt/sources.list.d/elastic-7.x.list'

Una vez agregado el repositorio instalar elasticsearch a través del siguiente comando

    sudo apt update  
    sudo apt install elasticsearch

Finalmente, habilite Elasticsearch para que se inicie en el arranque:  

    systemctl enable elasticsearch  
    systemctl start elasticsearch  

Utilice los siguientes comandos para iniciar, detener o reiniciar el nuevo servicio

    systemctl start elasticsearch  
    systemctl stop elasticsearch  
    systemctl restart elasticsearch  


### 3.3 Instalación y configuración de Postgresql + Postgis ###

Agregar repositorio de postgresql a nuestro servidor:

    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - RELEASE=$(lsb_release -cs)  
    echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee /etc/apt/sources.list.d/pgdg.list  

Actualizar repositorios de ubuntu:

    apt update

Instalar paquete postgresql a través del siguiente comando:

    apt -y install postgresql-13

Verificar que el servicio se encuentre corriendo:

    ss -tunelp | grep 5432

Crear contraseña para usuario default:

    su postgres  
    psql -c "alter user postgres with password 'NewPassword'"  
    exit

Instalar postgis:  

    apt install postgis postgresql-13-postgis-2.5

Probar instalación de postgis:

    sudo -i -u postgres  
    createuser postgis_test  
    createdb postgis_db -O postgis_test  
    psql -d postgis_db  
    CREATE EXTENSION postgis;  
    SELECT PostGIS_version(); →  2.5 USE_GEOS=1 USE_PROJ=1 USE_STATS=1  
  

Comandos del servicio:

    sudo systemctl restart|start|stop postgresql

### 3.4 Instalación y configuración de Tomcat ###

Instalar paquete OpenJDK a través del siguiente comando:

    apt install openjdk-11-jdk
    apt install default-jdk

Creación de usuario tomcat, por razones de seguridad, tomcat no debe ejecutarse bajo el usuario root. Crearemos un nuevo usuario y grupo del sistema con el directorio de inicio /opt/tomcat que ejecutará el servicio:

    useradd -r -m -U -d /opt/tomcat -s /bin/false tomcat

Descargaremos la última versión de Tomcat 9. Antes de continuar con el siguiente paso, debe consultar la página de descarga para obtener una nueva versión. Si hay una nueva versión, copie el enlace al archivo core tar.gz, que se encuentra en la sección distribuciones binarias

    wget wget https://downloads.apache.org/tomcat/tomcat-9/v9.0.62/bin/apache-tomcat-9.0.62.tar.gz -P /tmp

Una vez que se complete la descarga, extraiga el archivo Tomcat y muévalo al directorio /opt/tomcat:

    tar xf /tmp/apache-tomcat-9\*.tar.gz -C /opt/tomcat

Para tener más control sobre las versiones y actualizaciones de tomcat, cree un enlace simbólico que apunte al director de instalación de tomcat:

    ln -s /opt/tomcat/apache-tomcat-9.0.62 /opt/tomcat/latest

  
Cambiar la propiedad del directorio de tomcat al usuario y grupos creados anteriormente:  

    chown -R tomcat:tomcat /opt/tomcat/latest

Asignar permisos de ejecución a los binarios de la instalación:

    sh -c 'chmod +x /opt/tomcat/latest/bin/*.sh'

Para ejecutar tomcat como un servicio, crear un nuevo archivo ‘unit file’:

    nano /etc/systemd/system/tomcat.service

Pegar la siguiente configuración:

    [Unit]

        Description=Tomcat 9 servlet container  
        After=network.target  

    [Service]  
    
        Type=forking  
        User=tomcat  
        Group=tomcat  
        Environment="JAVA_HOME=/usr/lib/jvm/default-java"  
        Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom -Djava.awt.headless=true"  
        Environment="CATALINA_BASE=/opt/tomcat/latest"  
        Environment="CATALINA_HOME=/opt/tomcat/latest"  
        Environment="CATALINA_PID=/opt/tomcat/latest/temp/tomcat.pid"  
        Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"  
        ExecStart=/opt/tomcat/latest/bin/startup.sh  
        ExecStop=/opt/tomcat/latest/bin/shutdown.sh  

    [Install]  
    
        WantedBy=multi-user.target  

*\*Modificar el valor de JAVA_HOME si la ruta a su instalación de Java es diferente*  
<br>  

Guardar, cerrar el archivo y notificar a systemd que creamos un nuevo archivo de unidad:

    systemctl daemon-reload

Iniciar el servicio tomcat ejecutando:

    systemctl start tomcat
  
Verificar el estado del servicio con el siguiente comando:

    sudo systemctl status tomcat
  
Si no hay errores, habilite el servicio Tomcat para que se inicie automáticamente en el momento del arranque:

    sudo systemctl enable tomcat
  
Probar instalación de tomcat, para esto, acceder vía el navegador a la ip de nuestro servidor a través del puerto 8080, se tendrá que visualizar la página de bienvenida de apache tomcat como se visualiza a continuación:

![Screenshot](images/installation/image8.png)
> Figura 2. Apache tomcat, Code Robot (2022).
  
Comandos del servicio:

    sudo systemctl restart|start|stop tomcat


### 3.5 Instalación y configuración de MiMercadoAMG Web ###
---------------------------------------------------

Crear base de datos de MIMercadoAMG y sus credenciales de acceso:

    sudo su postgres  
    createuser mimercado
    createdb mimercado -O mimercado  
    psql -d mimercado
    alter user mimercado with password 'mimercadoamg_pass';  
    CREATE EXTENSION postgis;  
    CREATE EXTENSION postgis_topology;  

Asignar variables de entorno de la aplicación en el archivo catalina.properties dentro de la instalación de tomcat:  

Paso 1, acceder archivo de configuración:

    sudo nano /opt/tomcat/apache-tomcat-9.0.62/conf/catalina.properties

Paso 2, pegar al final del archivo las siguientes variables de configuración:

__# Spring - General__  

    spring.profiles.active=prod,swagger  
    server.use-forward-headers=true  
    spring.http.multipart.max-file-size=50MB  
    spring.http.multipart.max-request-size=50MB  
    # This token must be encoded using Base64 and be at least 256 bits long (you can type # \`openssl rand -base64 64\` on your command line to generate a 512 bits one)  
    jhipster.security.authentication.jwt.base64-secret=secret-hash  

__# Database configuration__  

    spring.datasource.url=jdbc:postgresql://localhost:5432/mimercadoamg_db  
    spring.datasource.username=mimercadoamg_user  
    spring.datasource.password=mimercadoamg_pass  

__# MinIo configuration__  

    minio.url=http://127.0.0.1:9001  
    minio.access.root-user=mi-mercado-amg-random-key  
    minio.access.root-password=mi-mercado-amg-random-secret-key  

__# ElasticSearch configuration__  

    spring.elasticsearch.rest.uris=http://localhost:9200  

__# Application configurations__  

    application.base-url=https://mimercadoamg.com.mx  
    application.tmp-file-directory=/tmp  
    application.enabled-cart=true  
    application.geo-delivery-validation=true  
    application.mobile-app-version=1.8  
    application_mobile-redirect=mimercadoamg://login  
  
__# Third-party authentication__  

Opcionalmente puede activarse Facebook como método de autenticación, realizar los siguientes pasos para la obtención de id de cliente y secreto:

1. Accede al enlace https://developers.facebook.com.
2. En la parte superior derecha, haz clic en Mis aplicaciones.
3. Crear una aplicación de inicio de sesión .
4. En la ventana emergente, ingrese el nombre para mostrar y el correo electrónico de contacto. Luego haga clic en crear ID de aplicación.
5. En el panel izquierdo, haga clic en Configuración -> Básico.
6. El ID de la aplicación (client-id) y el secreto de la aplicación (client-secret) se mostrarán aquí.
7. Acceder a la opción de inicio de sesión de Facebook en la sección productos.
8. Haga clic en configuración en el inicio de sesión de Facebook.
9. En el campo URI de redireccionamiento de OAuth válido, ingrese la siguiente URI: 
[https://mimercadoamg.com.mx/login/oauth2/code/facebook?mobile=true](https://mimercadoamg.com.mx/login/oauth2/code/facebook?mobile=true)  
[https://mimercadoamg.com.mx/login/oauth2/code/facebook](https://mimercadoamg.com.mx/login/oauth2/code/facebook)

&ensp;10\. En el campo dominios permitidos para el SDK para JavaScript.  
&ensp;&ensp;[https://mimercadoamg.com.mx/](https://mimercadoamg.com.mx/)  
  
\* Configuración representativa, revisar documentación oficial de Facebook para obtención de client-secrets. 

    application.client.social-login.facebook-enabled=false  
    spring.security.oauth2.client.registration.facebook.client-id=client 
    spring.security.oauth2.client.registration.facebook.client-secret=secret  

Opcionalmente puede activarse Google como método de autenticación, para crear un ID de cliente y un secreto de cliente, cree un proyecto de consola de API de Google, configure un ID de cliente de OAuth y registre sus orígenes de JavaScript:

1. Vaya a la consola de API de Google.
https://console.developers.google.com
2. En el menú desplegable del proyecto, seleccione un proyecto existente o cree uno nuevo seleccionando Crear un proyecto nuevo.
3. En la barra lateral debajo de "API y servicios", seleccione Credenciales y luego haga clic en Configurar pantalla de consentimiento.  
   a. Elija una dirección de correo electrónico, especifique un nombre de producto y presione Guardar.
4. En la pestaña Credenciales, seleccione la lista desplegable Crear credenciales y elija ID de cliente OAuth.
5. En Tipo de aplicación, seleccione Aplicación web.  
Registre los orígenes desde los que su aplicación puede acceder a las API de Google, de la siguiente manera. Un origen es una combinación única de protocolo, nombre de host y puerto.  
    a. En el campo Orígenes autorizados de JavaScript mantenerlo vacío.  
    b. En el campo URI de redireccionamiento autorizado ingresar:  
    [https://mimercadoamg.com.mx/login/oauth2/code/google](https://mimercadoamg.com.mx/login/oauth2/code/google)
    [https://mimercadoamg.com.mx/login/oauth2/code/google?mobile=true](https://mimercadoamg.com.mx/login/oauth2/code/google?mobile=true)    
    c. Presione el botón crear.
6. En el cuadro de diálogo del cliente OAuth resultante, copie el ID de cliente. El ID de cliente permite que su aplicación acceda a las API de Google habilitadas.

\* Configuración representativa, revisar documentación oficial de Google para obtención de client-secrets.

    application.client.social-login.google-enabled=false
    spring.security.oauth2.client.registration.google.client-id=client
    spring.security.oauth2.client.registration.google.client-secret=secret


Opcionalmente puede activarse Apple como método de autenticación, ejecutar los siguientes pasos para obtener el ID de cliente y la clave secreta:

1. Para configurar un cliente generar cuenta de desarrollador en:  
https://developer.apple.com
2. Inicie sesión y haga clic en "Certificados, identificadores y perfiles".
3. A continuación, cree un nuevo identificador de ID de aplicación y habilite "Iniciar sesión con Apple".
4. Después de eso, crear un nuevo Identificador de servicio. Este identificador de servicio será la identificación del cliente. Tenga en cuenta que las URL de retorno configuradas deben ejecutar TLS y estar configuradas en el puerto 443.  
[https://mimercadoamg.com.mx/login/oauth2/code/apple](https://mimercadoamg.com.mx/login/oauth2/code/apple)  
[https://mimercadoamg.com.mx/login/oauth2/code/apple?mobile=true](https://mimercadoamg.com.mx/login/oauth2/code/apple?mobile=true)  
5. El siguiente paso es crear una clave que se utilizará para generar su secreto de cliente. Habilite esta clave para usarla con Apple Sign In. Descargar esta clave y asignarle el nombre key.txt.
6. Guardar el script ‘client-secret.rb’ incluido en la carpeta de binarios del proyecto en la misma ubicación que el archivo key.txt,  editar el script preconfigurando las siguientes variables en base a la cuenta de desarrollador:  
    \# Your 10-character Team ID  
    team_id = ''  
    \# Your Services ID, e.g. com.aaronparecki.services  
    client_id = ''  
    \# Find the 10-char Key ID value from the portal  
    key_id = ''  
7. Instalar ruby y ejecutar el comando gem install jwt para obtener la dependencia jwt requerida por el script client-secret.rb.
8. Ejecute el script ejecutando el siguiente comando:  
ruby client_secret.rb

\* Esto generará un token JWT que se utilizará como secreto de cliente.

    application.client.social-login.apple-enabled=false  
    spring.security.oauth2.client.registration.apple.client-id=client  
    spring.security.oauth2.client.registration.apple.client-secret=secret  
    
     
    

__# Notification push / Firebase__

Mi Mercado utiliza Google Firebase Cloud Messaging para el envío de notificaciones push, para obtener las API Key de acceso es necesario crear un proyecto de firebase desde el siguiente enlace:

[https://console.firebase.google.com/u/0/?pli=1](https://console.firebase.google.com/u/0/?pli=1)

Una vez creado ejecutar los siguientes pasos:
  
![Screenshot](images/installation/image7.png)  
> Figura 3. Referencia Panel de Firebase, Code Robot (2022).
  
1.  Acceder al apartado de configuración del proyecto
2.  Acceder al apartado de cuentas de servicio
3.  Acceder al apartado de otras cuentas de servicio

  
En el apartado otras cuentas de servicio encontrarás la siguiente distribución de contenido:

![Screenshot](images/installation/image12.png) 
> Figura 4. Referencia de cuentas de servicio, Code Robot (2022).  

1.  Prestar atención a la cuenta con dominio @appspot.gserviceaccount.com
2.  Acceder a la opción 'Create key'

![Screenshot](images/installation/image9.png)
> Figura 5. Referencia KeyType, Code Robot (2022).  

Seleccionar la opción de crear una nueva clave de tipo JSON  
Este último paso generará un archivo de tipo JSON, abrir en cualquier editor de texto y recuperar de este las variables de configuración

![Screenshot](images/installation/image10.png)
> Figura 6. Referencia configuración, Code Robot (2022).  

Asignar los atributos como se detalla a continuación:

1.  El valor PROJECT_ID para la variable de entorno application.firebase.app-id (a)  
2.  El valor CLIENT_EMAIL en la variable de entorno application.firebase.account-id (b)  
3.  El valor PRIVATE_KEY en la variable de entorno application.firebase.private-key (c)

\* Asignar a

    application.firebase.app-id=  
    application.firebase.account-id=firebase-adminsdk-qikfi@mimercad…  
    application.firebase.private-key=

__# Recaptcha V3__  

Mi Mercado utiliza Recaptcha V3 para la detección de bots, para obtener las API Key de acceso es necesario crear un proyecto desde el siguiente enlace:  

[https://www.google.com/recaptcha/admin/create](https://www.google.com/recaptcha/admin/create)

![Screenshot](images/installation/image2.png)
> Figura 7. Configuración reCapcha, Code Robot (2022).  

    application.recaptcha-v3.secret-key=  
    application.recaptcha-v3.pass-score=0.7  
    application.recaptcha-v3.enabled=true

__# Mail output configuration__  

    jhipster.mail.from=mail@mail.com  
    jhipster.mail.base.url=https://mimercadoamg.com.mx  
    spring.mail.host=smtp.gmail.com  
    spring.mail.port=587  
    spring.mail.username=  
    spring.mail.password=  
    spring.mail.protocol=smtps  
    spring.mail.tls=true  
    spring.mail.properties.mail.smtp.auth=true  
    spring.mail.properties.mail.smtp.starttls.enable=true  
    spring.mail.properties.mail.smtp.ssl.trust=smtp.gmail.com  

Paso 3, reiniciar tomcat para que los cambios surtan efecto:

    sudo systemctl restart tomcat

Clonar repositorio de binarios  
    
    cd /tmp    
    git clone https://gitlab.com/coderobot-giz-mmamg/mi-mercado-webapp-binaries.git  
    cd mi-mercado-webapp-binaries

Depositar última versión de binario en el directorio de webapps de tomcat:
    
    rm -R  /opt/tomcat/latest/webapps/*  
    cp mimercado-latest.war /opt/tomcat/latest/webapps/ROOT.war

Verificar logs de tomcat para determinar estado de la instalación:  

    tail -f /opt/tomcat/latest/logs/catalina.out  

![Screenshot](images/installation/image17.png)  
> Figura 8. Imagen de referencia, Code Robot (2022). 

Probar instalación de MiMercadoAMG, para esto, acceder vía el navegador a la ip de nuestro servidor a través del puerto 8080, se tendrá que visualizar la página de bienvenida de MiMercadoAMG

Para ingresar probar usuario y contraseñas default:

    user: admin  
    password: password  

![Screenshot](images/installation/image1.png)
> Figura 9. Imagen de referencia, Code Robot (2022). 


### 3.6 Instalación y configuración de Proxy NGINX ###
----------------------------------------------

  
Instalar nginx de los repositorios de ubuntu:
    
    apt install nginx  

Verificar estado de instalación:

    service nginx status  

Generación de certificados HTTPS:  

* Local o pruebas:

    mkdir -p /etc/nginx/certs  
    cd /etc/nginx/certs  
    openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=localhost" -keyout ./privkey.pem -out ./fullchain.pem

* Entorno productivo  
&emsp;# Generar certificado con proveedor de certificados y depositar  
&emsp;# archivos privkey.pem y fullchain.pem en la ruta /etc/nginx/certs  


Configuración de NGINX:  

    sudo unlink /etc/nginx/sites-enabled/default  
    cd /etc/nginx/sites-available/  
    nano mmamg.conf


Pegar la siguiente configuración en el archivo msedmetro.conf y guardar:  

    server {  
        listen 80;  
        location / {  
            proxy_set_header Host $host;  
            proxy_pass http://localhost:8080;  
        }  
    }  
    server {  
        listen 443;  
        ssl on;  
        ssl_certificate /etc/nginx/certs/fullchain.pem;  
        ssl_certificate_key /etc/nginx/certs/privkey.pem;  
        location / {  
            proxy_set_header Host $host;  
            proxy_pass http://localhost:8080;  
        }  
    }  

Continuar con la configuración de NGINX:

    ln -s /etc/nginx/sites-available/mmamg.conf /etc/nginx/sites-enabled/mmamg.conf
    sudo service nginx restart

Probar instalación, para esto, acceder vía el navegador a la ip de nuestro servidor a través del puerto 80 o 443, se tendrá que visualizar la página de bienvenida de MiMercadoAMG.



### 3.7 Preparación de Aplicación Móvil ###

Para compilar y publicar la aplicación iOS y la aplicación Android se requiere de un equipo OSX con xCode, Android Studio y NodeJS 16 LTS instalado, una vez satisfechas las dependencias anteriores proceder como se detalla a continuación:

#### 3.7.1 Configuración de aplicativo móvil ####

  
Revisar versiones de nodejs y npm

    node --version  
    npm --version  

Instalar dependencias de entorno

    npm install -g @ionic/cli

Obtener código fuente repositorio  

    git clone https://gitlab.com/coderobot-giz-mmamg/mi-mercado-ionic.git

Instalar dependencias de aplicativo  

    cd mi-mercado-ionic  
    npm install
  
Actualizar variables de entorno de la aplicación móvil

    nano src/environments/environment.prod.ts  
    nano src/environments/environment.ts

  
![Screenshot](images/installation/image15.png)
> Figura 10. Imagen de referencia, Code Robot (2022).
  

Agregar credenciales de Firebase Cloud Messaging, a partir de la aplicación Firebase definida en el apartado anterior, ingresar a la misma y agregar una nueva aplicación Android como se muestra a continuación:

![Screenshot](images/installation/image18.png)
> Figura 11. Imagen de referencia, Code Robot (2022). 

Configurar el nombre de paquete y la información de manera consecuente a las configuraciones anteriores:

![Screenshot](images/installation/image3.png)
> Figura 12. Imagen de referencia, Code Robot (2022). 

Descargar el archivo de configuración google-services.json y almacenarlo en el siguiente directorio:

![Screenshot](images/installation/image5.png)
> Figura 13. Imagen de referencia, Code Robot (2022). 

    android/app/google-services.json

*\*Una vez obtenido este archivo no es necesario continuar con los pasos siguientes dentro del asistente de  firebase*

Para la versión iOS, agregar una nueva aplicación iOS como se muestra a continuación:

![Screenshot](images/installation/image6.png)
> Figura 14. Imagen de referencia, Code Robot (2022).

Configurar el nombre de paquete y la información de manera consecuente a las configuraciones anteriores:

![Screenshot](images/installation/image3.png)
> Figura 15. Imagen de referencia, Code Robot (2022).

Descargar el archivo de configuración GoogleService-Info.plist y almacenarlo en el siguiente directorio:

![Screenshot](images/installation/image14.png)
> Figura 16. Imagen de referencia, Code Robot (2022).

    ios/App/App/GoogleService-Info.plist

#### 3.7.2 Ejecutar la aplicación / Android  ####
  
Para ejecutar la aplicación utilizar el siguiente comando:
  
    ionic capacitor build android --prod

*\*Esto lanzará Android Studio con el proyecto cargado, con lo cual podrás ejecutar la aplicación utilizando la documentación oficial de Android Studio*

[https://developer.android.com/training/basics/firstapp/running-app](https://developer.android.com/training/basics/firstapp/running-app)

#### 3.7.3 Ejecutar la aplicación / iOS  ####
  
Para ejecutar la aplicación utilizar el siguiente comando:

    ionic capacitor build ios --prod

*\*Esto lanzará XCode con el proyecto cargado, con lo cual podrás ejecutar la aplicación utilizando la documentación oficial de XCode*

[https://developer.apple.com/documentation/xcode/running-your-app-in-the-simulator-or-on-a-device](https://developer.apple.com/documentation/xcode/running-your-app-in-the-simulator-or-on-a-device)

#### 3.7.4 Publicar aplicación #######

Referirse a la documentación oficial de las respectivas tiendas:

Android:  
[https://developer.android.com/studio/publish](https://developer.android.com/studio/publish)

iOS:

[https://developer.apple.com/documentation/xcode/preparing-your-app-for-distribution](https://developer.apple.com/documentation/xcode/preparing-your-app-for-distribution)

__*Fecha de actualización: 21-04-2022*__