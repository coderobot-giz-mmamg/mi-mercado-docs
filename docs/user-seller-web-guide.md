# Guía del vendedor web #

## 1. Introducción ##


### 1.1 Alcance de este documento ###


El propósito de este manual es explicar de manera detallada desde el usuario de vendedor el manejo de la solución tecnológica de Mi Mercado AMG.

### 1.2 Cómo utilizar este manual ######

El presente manual ha sido redactado para ser un documento de referencia para el rol de usuario vendedor.

El manual está construido a partir de dos grandes temas:

1.  Distribución general: Corresponde a la explicación general de la plataforma pública, así como, el inicio de sesión y recuperación de cuenta.  
2.  Explicación de conjuntos de módulos: Especificación de funcionalidades particulares de cada módulo.

Cabe mencionar que para efectos de desarrollo del presente manual todas las figuras y explicaciones de flujo están basadas en el tipo de usuario “Vendedor”.

* * *

## 2\. Distribución general ##


### 2.1 Página principal ###

La plataforma se compone de tres regiones principales:

La primera es el encabezado de la página principal, esta muestra el logo de Mi Marcado AMG, carrusel de banners para publicaciones importantes, carrito de compra, buscador de productos, así como, el acceso directo para el inicio de sesión.

![Screenshot](images/seller-web-guide/image38.png)
> Figura 1. Encabezado, Code Robot (2022).  

La segunda es el contenido, este apartado muestra las categorías y tiendas vistas anteriormente.

![Screenshot](images/seller-web-guide/image34.png)
> Figura 2. Contenido público del home, Code Robot (2022).

La tercera parte es el pie de página, el cual mostrará la información de contacto de Mi Mercado AMG.

![Screenshot](images/seller-web-guide/image19.png)
> Figura 3. Pie de página, Code Robot (2022).

### 2.2 Inicio de sesión ###

Los usuarios con cuenta podrán acceder a la plataforma a través de la opción “Inicio de sesión”, la cual se muestra en el header del sitio:

![Screenshot](images/seller-web-guide/image24.png)
> Figura 4. Botón inicio de sesión, Code Robot (2022).


Al ejecutar esta opción la plataforma mostrará una ventana donde se podrán introducir los datos de acceso del usuario.

![Screenshot](images/seller-web-guide/image33.png)
> Figura 5. Ventana inicio de sesión, Code Robot (2022).

1.  Iniciar sesión como “Vendedor”.
2.  Inicio de sesión: Permite al vendedor iniciar sesión después de ingresar sus datos.
3.  Recuperar contraseña: Permite al usuario recuperar su contraseña a través de su correo.
4.  Crear cuenta: Este apartado permite la creación de cuentas de usuario.
5.  Inicio de sesión con Google: Permite al usuario iniciar sesión con su cuenta de Google.  
6.  Inicio de sesión con Apple: Permite al usuario iniciar sesión con su cuenta de Apple.


#### 2.2.1 Recuperación de contraseña ####

En caso de olvidar la contraseña, en el apartado anterior aparece una pregunta “¿Olvidaste tu contraseña?”, esta opción dirigirá al siguiente formulario de recuperación de contraseña, en donde se introduce el correo electrónico con el cual fue dada de alta la cuenta en la plataforma.

![Screenshot](images/seller-web-guide/image23.png)
> Figura 6. Recuperación de contraseña, Code Robot (2022).

Al dar clic en el botón “Restablecer contraseña”, la plataforma enviará un correo electrónico con una llave temporal de acceso mediante el cual se podrá modificar la contraseña, por otra parte el sistema emitirá un mensaje al instante que se envió con éxito el correo de recuperación.

![Screenshot](images/seller-web-guide/image25.png)
> Figura 7. Mensaje de recuperación de contraseña, Code Robot (2022).

* * *

## 3\. Cuenta ##


El usuario podrá ingresar a la plataforma y según sus permisos asignados podrá hacer uso de los módulos de administración que se presentan a continuación:

![Screenshot](images/seller-web-guide/image31.png)
> Figura 8. Menú de administración de cuenta, Code Robot (2022).

### 3.1 Ajustes ###

Permite al usuario modificar la información básica de su perfil, por ejemplo:

*   Nombre(s): Campo de texto, permite capturar de 1 a 50 caracteres, obligatorio.
*   Apellidos: Campo de texto, permite capturar de 1 a 50 caracteres, obligatorio.
*   Teléfono: Campo numérico, obligatorio.
*   Correo electrónico: Campo de email, permite capturar de 5 a 100 caracteres, obligatorio.

![Screenshot](images/seller-web-guide/image20.png)
> Figura 9. Ajustes del usuario, Code Robot (2022).

### 3.2 Contraseña ###

Permite al usuario modificar la contraseña de acceso a la plataforma, para poder realizar esta acción es necesario contar con la contraseña actual. Es un campo obligatorio.

Contraseña actual / nueva contraseña: Campo de texto, permite capturar de 4 a 50 caracteres.

![Screenshot](images/seller-web-guide/image12.png)
> Figura 10. Ajuste de contraseña, Code Robot (2022).

### 3.3 Cerrar sesión ###

Finaliza la sesión del usuario dentro de la plataforma, al hacerlo la plataforma redireccionará a la página principal del sitio.

* * *

## 4\. Panel de administración ##

El siguiente panel administrativo permite al vendedor tener una forma rápida de acceso a los módulos de consulta y administración. Para ingresar a este apartado el usuario deberá dar clic “Panel de perfil” en el menú que se localiza en el header de la plataforma.

![Screenshot](images/seller-web-guide/image13.png)
> Figura 11. Menú general, Code Robot (2022).

![Screenshot](images/seller-web-guide/image5.png)
> Figura 12. Panel administrativo, Code Robot (2022).

### 4.1 Creación de tienda ###

Este módulo permite al vendedor  editar y añadir nuevas tiendas, así como, consultar productos, órdenes y preguntas.

Al acceder a este módulo de “Mis tiendas”  la plataforma desplegará una pantalla donde se mostrará ya sea el listado de tiendas previamente capturadas o la creación de una nueva tienda.

![Screenshot](images/seller-web-guide/image16.png)
> Figura 13. Mis tiendas, Code Robot (2022).

Al ingresar al formulario de crear tienda, la plataforma mostrará la primer parte de captura de la misma, los campos se presentan en la siguiente figura :

1.  Nombre del negocio: El vendedor deberá escribir el nombre de su negocio.
2.  Mercado: Seleccionar el nombre del mercado donde se encuentra el negocio.
3.  Código de seguridad: Este código será proporcionado por el administrador o responsable de la plataforma.

![Screenshot](images/seller-web-guide/image9.png)
> Figura 14. Crear tienda, Code Robot (2022).

Una vez capturada correctamente la información anterior, se mostrará la segunda parte del formulario de captura de una nueva tienda. La siguiente figura muestra los campos:

&ensp;4\.  Dirección o número de local: El vendedor deberá indicar el lugar donde pueden pasar a recoger sus pedidos los usuarios.  
&ensp;5\.  Horario de atención: Este campo hace referencia al horario de atención a pedidos en la plataforma, este no necesariamente tiene que ser el horario real en el que habitualmente abre el negocio.

![Screenshot](images/seller-web-guide/image17.png)
> Figura 15. Formulario de creación de tienda, Code Robot (2022).

&ensp;6\.  Métodos de entrega: El vendedor deberá seleccionar al menos un método de entrega, puede ser a domicilio o entrega en mercado (pickup).  
&ensp;7\.  Repartidores asignados: Al haber seleccionado entrega a domicilio, el vendedor podrá seleccionar sus propios repartidores previamente dados de alta.  
&ensp;8\.  Repartidores globales: Hace referencia a los repartidores proporcionados por el mercado, esto en caso de no contar con repartidores exclusivos.  
&ensp;9\.  Política de envío: Este apartado permite al vendedor especificar alguna política de sus envíos, estos pueden ser los tiempos de entrega, etc.  
&ensp;10\.   Entregas en mercado: Este es el segundo método de entrega, el cual hace referencia a que los compradores podrán pasar por sus pedidos listos al local.  
&ensp;11\.   Estado de la tienda: Este campo podrá ser marcado como activo si se quiere que la tienda sea visible para los usuarios en la plataforma.  
&ensp;12\.   Formas de pago aceptadas: Este campo es un checkbox donde se podrán seleccionar los métodos de pago válidos en la tienda, estos pueden ser efectivo o a través de una pasarela de pago (recordar consultar con el administrador antes de activar una pasarela de pago ya que requiere la generación de una cuenta en dicha pasarela).  
&ensp;13\.   Terminal punto de venta: Marcar como activado en caso de contar con terminal de pagos en punto de venta.  
&ensp;14\.   Imagen / logo: Permite al usuario subir el logo del negocio, en caso de no contar con logo, pudiera subir alguna foto del negocio como referencia visual para los compradores.  
&ensp;15\.  Giro / ramo industrial: Seleccionar el ramo en el cual se ubica el negocio.  
&ensp;16\.  Bloquear entrada de nuevas órdenes: Permite al vendedor cerrar la opción de que sus productos puedan ser añadidos al carrito de compra.  
&ensp;17\.  Configurar horario de atención: Permite calendarizar el horario de atención del negocio.  
&ensp;18\.  Configurar horario de envío a domicilio: Permite al usuario añadir horario específico de envío de productos a domicilio.  
&ensp;19\.  Guardar: Permite al usuario guardar la tienda con todos los campos ya capturados.  

![Screenshot](images/seller-web-guide/image6.jpg)
> Figura 16. Formulario de creación de tienda 2, Code Robot (2022).

![Screenshot](images/seller-web-guide/image36.jpg)
> Figura 17. Formulario de creación de tienda 3, Code Robot (2022).

### 4.2. Productos ###

Este apartado es una herramienta que permite al vendedor editar y dar de alta nuevos productos. Para acceder a este apartado es necesario estar en la sección de mis tiendas y dar clic en “Productos”.

![Screenshot](images/seller-web-guide/image26.png)
> Figura 18. Productos, Code Robot (2022).

Al acceder a esta sección la plataforma desplegará una pantalla donde se mostrará el listado de productos existentes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre.  
    c.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Crear nuevo producto: Permite al administrador acceder al formulario de creación de nuevo producto.
3.  Paginador: Permite navegar entre el listado de productos.


![Screenshot](images/seller-web-guide/image11.png)
> Figura 19. Gestión de productos, Code Robot (2022).


Las siguientes figuras muestran el formulario de creación o edición de productos:
![Screenshot](images/seller-web-guide/image8.png)

> Figura 20. Formulario de captura de productos 1, Code Robot (2022).

&ensp;4\.  Showroom: Seleccionar el check si lo capturado corresponde a un servicio el cual no cuente con un precio fijo, a continuación se muestran las siguientes figuras de ejemplo:  
&ensp;&ensp;a\.  Producto normal con precio

  
![Screenshot](images/seller-web-guide/image35.png)  
> Figura 21. Ejemplo de producto normal, Code Robot (2022).

&ensp;&ensp;b\.  Servicio o producto con precio variable  
    

![Screenshot](images/seller-web-guide/image28.png)
> Figura 22. Ejemplo servicio o producto, Code Robot (2022).

![Screenshot](images/seller-web-guide/image3.jpg)
> Figura 23. Formulario de captura de productos 2, Code Robot (2022).

&ensp;&ensp;5\.  Variantes y componentes: Permite añadir variantes del mismo producto que no afecten al precio, por ejemplo, chilaquiles; Categoría  = “Sabor”, Variante(s) = “rojos, verdes”.  
&ensp;&ensp;6\.  Añadir archivos: Permite seleccionar y adjuntar la imagen correspondiente.  
&ensp;&ensp;7\.  Guardar: Al finalizar el llenado de los campos el administrador o vendedor podrá guardar el nuevo producto.

### 4.3. Órdenes ###

Este módulo permite al vendedor consultar el listado de órdenes que han sido creadas por los usuarios.

![Screenshot](images/seller-web-guide/image18.png)
> Figura 24. Órdenes, Code Robot (2022).

Al acceder a esta sección la plataforma desplegará una pantalla donde se mostrará el listado de órdenes, las cuales se distribuyen de la siguiente manera:

1.  Filtros de búsqueda: La búsqueda por filtros ayuda a reducir la información mostrada de acuerdo a los parámetros siguientes:  
    a.  Búsqueda por ID.  
    b.  Búsqueda por nombre del cliente.  
    c.  Búsqueda por apellido del cliente.  
    d.  Al hacer clic en el botón buscar se aplicarán los parámetros seleccionados, por otra parte el botón limpiar eliminará los parámetros ingresados y el listado retomará los ajustes por default.
2.  Estatus de la compra: Permite al usuario revisar en todo momento el estatus de la orden, esta puede tener diferentes estatus, tales como:  
    a.  Orden creada  
    b.  Orden aceptada  
    c.  Preparando orden  
    d. Despachando orden  
    e. En espera de repartidor  
    f. Tu pedido está en camino  
    g. En puerta con el cliente  
    h. Entregado
3. Detalle de orden: Permite entrar a la información completa del pedido.
4. Paginador: Permite navegar entre el listado de órdenes.

![Screenshot](images/seller-web-guide/image14.png)
> Figura 25. Gestión de órdenes, Code Robot (2022).

Dentro del detalle de orden el vendedor podrá realizar las siguientes acciones:

&ensp;5\.  Detalles del pedido: Permite al vendedor consultar los detalles del pedido, tal como el método de entrega, número de pedido, domicilio, método de pago, etc.  
&ensp;6\.  Lista de productos solicitados: Muestra todos los productos del pedido, cantidad a despachar y el precio final.  
&ensp;7\.  Chat con el cliente: Esta herramienta permite al comprador y vendedor preguntar dudas y tener comunicación directa para una mejor experiencia de usuario.  
&ensp;8\.  Llamada con el cliente: En el apartado web se habilitó un botón donde aparece el número telefónico del cliente, en caso de requerir realizar llamada.

![Screenshot](images/seller-web-guide/image1.png)
> Figura 26. Detalle de pedido, Code Robot (2022).

&ensp;9\.  Selector de estatus: Permite la selección del listado predeterminado de actualización de estatus.  
&ensp;&ensp;a\.  Orden creada  
&ensp;&ensp;b\.  Orden aceptada  
&ensp;&ensp;c\.  Preparando orden  
&ensp;&ensp;d\.  Despachando orden  
&ensp;&ensp;e\.  En espera de repartidor  
&ensp;&ensp;f\.  Tu pedido está en camino  
&ensp;&ensp;g\.  En puerta con el cliente  
&ensp;&ensp;h\.  Entregado  

&ensp;10\.  Agregar mensajes de estatus: Permite al vendedor agregar alguna nota opcional en cada cambio de estatus de la orden. Ej ”Compra mediante terminal”.  
&ensp;11\.  Actualizar estatus: Después de seleccionar el estatus correcto el vendedor deberá dar clic en el botón de “Actualizar estatus”.

![Screenshot](images/seller-web-guide/image27.png)
> Figura 27. Estatus de órdenes, Code Robot (2022).

### 4.4 Preguntas ###

Este apartado es una herramienta que permite al vendedor consultar las preguntas que los usuarios hacen sobre los productos o servicios.

![Screenshot](images/seller-web-guide/image10.png)
> Figura 28. Gestión de preguntas, Code Robot (2022).

Al acceder a este apartado de “Preguntas” el sistema desplegará una pantalla donde se mostrará el listado de todas las preguntas que han generado los usuarios, las cuales se muestran en dos pestañas, de la siguiente manera:

1.  Sin responder: Se muestra el listado de preguntas pendientes de responder.
2.  Historial: Muestra el listado de preguntas ya respondidas.

![Screenshot](images/seller-web-guide/image2.png)
> Figura 29. Gestión de preguntas y respuestas, Code Robot (2022).

* * *

## 5\. Reportes ##

Este apartado es una herramienta que permite a los vendedores conocer el desempeño y datos históricos de su tienda, accediendo desde el panel administrativo.

![Screenshot](images/seller-web-guide/image29.png)
> Figura 30. Reportes, Code Robot (2022).

Al acceder a la sección de reportes es necesario seleccionar la tienda de la cual se quiere saber el reporte. La siguiente figura muestra el dropdown de selección de tienda:

![Screenshot](images/seller-web-guide/image4.png)
> Figura 31. Selección de tienda, Code Robot (2022).

Posterior a seleccionar la tienda se mostrará el reporte dividido en dos ámbitos: detalles generales y datos históricos, los cuales se muestran en la siguiente figura:

1.  Detalles generales: Este reporte muestra los resultados del mes actual, los cuales tienen los siguientes datos a mostrar.  
    a.  Desempeño  
    b.  Órdenes totales  
    c.  Órdenes pickup  
    d.  Órdenes a domicilio  
    e.  Preguntas  
    f.  Respuestas  
2.  Datos históricos: Los datos históricos hacen referencia a un periodo global o un periodo en específico.
3.  Filtros: Permiten realizar búsquedas de información de un periodo en específico.

![Screenshot](images/seller-web-guide/image22.png)
> Figura 32. Gestión de reportes, Code Robot (2022).

* * *

## 6\. Cargas masivas ##

Este apartado es una herramienta que permite al vendedor dar de alta nuevos productos de manera masiva, a través de archivos csv con formato específico.

![Screenshot](images/seller-web-guide/image30.png)
> Figura 33. Carga masiva, Code Robot (2022).

Al acceder desde el panel de administración al módulo de “Carga masiva de productos” el sistema desplegará una pantalla donde se mostrará el listado de importaciones masivas anteriores, y por otra parte el botón para crear una nueva importación, la cual se muestra de la siguiente manera:

1.  Tipo: Permite seleccionar el tipo de categoría que será, en este caso productos.
2.  Juego de caracteres: Al seleccionar el juego de caracteres a utilizar el sistema pone a disposición una plantilla y un diccionario de datos, de manera que puedas generar el archivo que se solicitará en el punto 3.
3.  Añadir archivos: Permite seleccionar y adjuntar el archivo csv correspondiente.

![Screenshot](images/seller-web-guide/image7.png)
> Figura 34. Creación de carga masiva, Code Robot (2022).

* * *

## 7\. Notificaciones ##

Esta herramienta permite al vendedor consultar las notificaciones lanzadas por la plataforma. Al dar clic en notificaciones ubicado en la barra de menú superior la plataforma desplegará un dropdown donde se mostrarán las notificaciones más nuevas.

![Screenshot](images/seller-web-guide/image32.png)
> Figura 35. Notificaciones, Code Robot (2022).

* * *

## 8. Consideraciones generales ##

Con la finalidad de dar cierre al presente manual del vendedor y dada la responsabilidad de uso de algunos de los módulos, a continuación se describen distintas responsabilidades que el vendedor deberá tener presente en todo momento:

1.  Actualización de estatus de compras: Al aceptar un pedido, se requiere en todo momento estar actualizando el estatus de la orden, esto brindará mayor confianza al cliente.
2.  Configuración del horario de atención: Para prevenir pedidos fuera de jornadas laborales, tanto de locatarios como del apartado de reparto.
3.  Configuración de métodos de pago: Recordar solo hacer uso de los métodos de pago aprobados, y en caso de requerir activar una pasarela de pagos, solicitar asistencia del equipo administrativo.
4.  Actualización de productos: Es necesario mantener actualizados los productos de la tienda, así como disponibilidad y precios de los mismos.
5.  Respuestas al comprador: En todo momento se deberá tener en cuenta que los clientes tendrán dudas de sus órdenes, así como el vendedor podrá ponerse en contacto con el cliente para resolver dudas del pedido.
6.  Asignación de repartidores: El vendedor que haya seleccionado envío a domicilio en su tienda, deberá asegurarse de tener activos los repartidores, ya sean exclusivos del negocio o repartidores globales del mercado.
7.  Notificar problemas al administrador vía formulario de contacto o correo.
8.  Revisar las políticas y términos de uso.
9.  Notificar al administrador de alta de sus repartidores propios.

__*Fecha de actualización: 21-04-2022*__